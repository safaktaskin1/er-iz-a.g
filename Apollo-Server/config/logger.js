const {createLogger,transports,format} = require('winston');
const moment = require('moment');
const path = require("path");
moment.locale('tr')
//  const deyt = () => 
//   new Date(Date.now().toLocaleString());

// console.log(Date.now().toLocaleString());
// const timezoned = () => {
//   return new Date().toLocaleString('tr', {
//     timeZone: 'Europe/Istanbul'
//   });
// }
const timezoned = () => {
  return moment().format('DD-MM-YYYY HH:mm:ss');
  
}
const logger = createLogger({
  
  transports:[

    
    new transports.File({
      filename: path.join(__dirname, './log/info.log'),
      

      // filename: path.join(__dirname, './log/error.log'),
      level:'error',
      // timestamp:deyt,
      // format: format.printf(info => `${deyt} ${info.message}`),
      format: format.combine(format.timestamp({format: timezoned}), format.simple())
    }),
    new transports.File({
      filename: path.join(__dirname, './log/error.log'),
      // filename: path.join(__dirname, './log/error.log'),
      level:'warn',
      // format: format.combine(format.timestamp({
      //   format: timezoned
      // }), format.simple())
      format: format.combine(
        format.timestamp(),
        format.prettyPrint()
      ),
    }),
    new transports.File({
      level:'http',
          filename: path.join(__dirname, './log/combined.log'),
    }),
    new transports.Console({
      filename: path.join(__dirname, './log/error.log'),
      level:'info',
      format: format.combine(format.timestamp({
        format: timezoned
      }), format.simple())
    })
  ],
  // exceptionHandlers: [
      
  //     new transports.File({
  //       // format: format.combine(
  //       //   format.timestamp(),
  //       //   format.prettyPrint()
  //       // ),
  //       level: 'silly',
  //       json: false,
  //       handleExceptions: false,
  //       filename: path.join(__dirname, './log/exceptions.log')
  //     })
  //   ]
});
module.exports = logger;