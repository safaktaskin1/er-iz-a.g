let passport = require('passport')
let localStrategy=require('passport-local')
let BearerStrategy=require('passport-http-bearer')
let bcrypt =require('bcryptjs')
import Models from ('../models')
// model
var JwtStrategy = require('passport-jwt').Strategy,
ExtractJwt = require('passport-jwt').ExtractJwt;

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
opts.issuer = 'accounts.examplesoft.com';
opts.audience = 'yoursite.net';

passport.use(new LocalStrategy(
    function(username, password, done) {
        Models.user.findOne({ where:{ email: email }}, function (err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        if (!user.verifyPassword(password)) { return done(null, false); }
        return done(null, user);
      });
    }
  ));
  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
      console.log("jwt_payload",jwt_payload);
    Models.user.findOne({ where:{id: jwt_payload.sub}}, function(err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
}));
passport.use(new BearerStrategy(
    function(token, done) {
      Models.user.findOne({ where:{ token: token }}, function (err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user, { scope: 'all' });
      });
    }
  ));