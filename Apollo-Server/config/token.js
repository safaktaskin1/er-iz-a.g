var jwt = require('jsonwebtoken');
require('dotenv').config();
const token ={
    generate:({firstname,email,role})=>{
        return jwt.sign({
          firstname,
          email,
          role
        }, process.env.JWT_SECRET
        // ,{
        //   expiresIn: 60
        // }
        );
    }
};

module.exports =token;