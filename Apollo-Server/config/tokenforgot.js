var jwt = require('jsonwebtoken');
require('dotenv').config();

const forgottoken ={
    generate:({email})=>{
        // console.log('email',email);
        // return jwt.sign({mail},process.env.JWT_SECRET),{ expiresIn: 60 * 6 };
        return jwt.sign({email},process.env.JWT_SECRET,{ expiresIn: 60*3});
    }
};
module.exports = forgottoken;