
const ejs = require('ejs');
require('dotenv').config();
const path = require("path");
const nodemailer = require('nodemailer');
import moment from 'moment'
// const template = './havale.ejs';
// let datam={};
let transporter = nodemailer.createTransport({
    host: "mail.eriza.com.tr",
    port: 587,
    secure: false,
      auth: {
        user: process.env.EMAIL, // generated ethereal user
        pass: process.env.EMAIL_PASS, // generated ethereal password
        // user:'bilgi@eriza.com.tr',
        // pass:'Eriza@!2021'
      },
      tls: {
        rejectUnauthorized: false
    }
  });
  
const checkedmail = async (datam)=>{
  // console.log("__dirname",__dirname);
  var date=moment().format('DD-MM-YYYY HH:mm');
await ejs.renderFile(__dirname + "/checkedmail.ejs", { 
  date:date,
  jobsid:datam.jobsid,
  usermail:datam.usermail
 }, function (err, data) {
  if (err) {
      console.log(err);
  } else {
      var mainOptions = {
          from: process.env.EMAIL,
          to: datam.usermail,
          subject: 'Eriza Bilişim Ticket Portalı',
          html:  data,
          attachments: [
            {
              filename: 'es.png',
              path: __dirname +'/images/es.png',
              cid: 'es.png'
            },
            {
              filename: 'Logo.png',
              path: __dirname +'/images/Logo.png',
              cid: 'Logo.png'
            },
        ],
      };

      transporter.sendMail(mainOptions, function (err, info) {
        if (err) {
          console.log(err);
          // err.Json({
          //   msg: 'fail'
          // })
        } else {
          console.log(info);
        }
    });
    }
});
}

exports.checkedmail = checkedmail