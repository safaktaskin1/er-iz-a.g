'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
      await queryInterface.createTable('company', { 
        id:{
          type:Sequelize.INTEGER,
          primaryKey:true,
          autoIncrement:true,
          allowNull:false
      },
      userId:{
        type:Sequelize.INTEGER,
        allowNull:true
      },
      cuserId:{
        type:Sequelize.INTEGER,
        allowNull:true
      },
      name:{
          type:Sequelize.STRING,
          allowNull:true
      },
      adress:{
          type:Sequelize.STRING,
          allowNull:true
      },

      email:{
          type:Sequelize.STRING,
          allowNull:true
      },
      pbx:{
          type:Sequelize.STRING,
          allowNull:true
      },
      fax:{
          type:Sequelize.STRING,
          allowNull:true
      },
      cep:{
          type:Sequelize.STRING,
          allowNull:true
      },
      il:{
          type:Sequelize.INTEGER,
          allowNull:true
      },
      ilce:{
          type:Sequelize.INTEGER,
          allowNull:true
      },
      decription:{
          type:Sequelize.STRING,
          allowNull:true
      },
      vergiDairesi:{
          type:Sequelize.STRING,
          allowNull:true
      },
      vergiNo:{
          type:Sequelize.STRING,
          allowNull:true
      },
      createdAt: 'TIMESTAMP',
      updatedAt: 'TIMESTAMP'
      
  
     
  
  })
  },
  down: async (queryInterface, Sequelize) => {
    
     await queryInterface.dropTable('company');
    
  }
};
