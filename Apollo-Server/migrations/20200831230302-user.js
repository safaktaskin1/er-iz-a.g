'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // console.log(Sequelize.models);
      await queryInterface.createTable('user', { 
        id:{
          type:Sequelize.INTEGER,
          primaryKey:true,
          autoIncrement:true,
          allowNull:false
        },
        firstname:{
            type:Sequelize.STRING,
            allowNull:true
        },
        lastname:{
            type:Sequelize.STRING,
            allowNull:true
        },

        email:{
            type:Sequelize.STRING,
            allowNull:true
        },
        password:{
            type:Sequelize.STRING
        },
        token:{
            type:Sequelize.STRING,
            allowNull:true
        },
        role:{
          type:Sequelize.STRING,
          default:'user'
        },
        companyId: {
          type: Sequelize.INTEGER,
          references: {        // User belongsTo Company 1:1
            model: 'company',
            key: 'id'
          }
        },
        createdAt: 'TIMESTAMP',
        updatedAt: 'TIMESTAMP'
      
      });
     
  },

  down: async (queryInterface, Sequelize) => {
    
     await queryInterface.dropTable('user');
    
  }
};
