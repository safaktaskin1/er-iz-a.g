'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('category', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      parentId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true
      },

      updatedAt: Sequelize.DATE,
      createdAt: Sequelize.DATE
    });

  },

  down: async (queryInterface, Sequelize) => {
    
     
      
      await queryInterface.dropTable('category');
     
  }
};
