'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('cuser', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      firstname: {
        type: Sequelize.STRING,
        allowNull: true
      },
      lastname: {
        type: Sequelize.STRING,
        allowNull: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: true
      },
      companyId: {
        type: Sequelize.INTEGER,
        references: {        // User belongsTo Company 1:1
          model: 'company',
          key: 'id'
        }
      },
      createdAt: 'TIMESTAMP',
      updatedAt: 'TIMESTAMP'

    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('cuser');
  }
};
