module.exports = (Sequelize, DataTypes)=>{

    const category = Sequelize.define('category',{
        categoryid: {
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true,
            allowNull:false
        },
        parentid:{
            type:DataTypes.INTEGER,
            allowNull:true,
        },
        name:{
            type:DataTypes.STRING,
            allowNull:true
        },
        
        updatedAt: DataTypes.DATE,
        createdAt: DataTypes.DATE
        
    },{
        hooks: {
           beforeCreate: function (category, options, fn) {
            category.createdAt = new Date();
            category.updatedAt = new Date();
            //    fn(null, category);
           },
           beforeUpdate: function (category, options, fn) {
            category.updatedAt = new Date();
            //    fn(null, category);
           }
       }
    })
    category.associate = function(models) {
        
            // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
            // company.hasMany(models.user, { sourceKey: 'id', foreignKey: 'companyId' })
            category.hasOne(models.departmentcategory,{sourceKey: 'categoryid', foreignKey: 'categoryid'})
            // category.hasOne(models.category,{sourceKey: 'parentid', foreignKey: 'parentid',as:'parent'})
        // };
      };
    // User.associate =function(models){
    //     User.Many(models.Post)
    // };
    return category;
}