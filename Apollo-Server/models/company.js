module.exports = (Sequelize, DataTypes)=>{

    const company = Sequelize.define('company',{
        companyid: {
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true,
            allowNull:false
        },
        userid:{
            type:DataTypes.INTEGER,
            allowNull:true
        },
        // cuserid:{
        //     type:DataTypes.INTEGER,
        //     allowNull:true
        // },

        name:{
            type:DataTypes.STRING,
            allowNull:true
        },
        adress:{
            type:DataTypes.STRING,
            allowNull:true
        },

        email:{
            type:DataTypes.STRING,
            allowNull:true
        },
        pbx:{
            type:DataTypes.STRING,
            allowNull:true
        },
        fax:{
            type:DataTypes.STRING,
            allowNull:true
        },
        cep:{
            type:DataTypes.STRING,
            allowNull:true
        },
        il:{
            type:DataTypes.INTEGER,
            allowNull:true
        },
        ilce:{
            type:DataTypes.INTEGER,
            allowNull:true
        },
        description:{
            type:DataTypes.STRING,
            allowNull:true
        },
        vergiDairesi:{
            type:DataTypes.STRING,
            allowNull:true
        },
        vergiNo:{
            type:DataTypes.STRING,
            allowNull:true
        },
        updatedAt: DataTypes.DATE,
        createdAt: DataTypes.DATE,
        // freezeTableName: true,
    },{
        hooks: {
           beforeCreate: function (company, options, fn) {
            company.createdAt = new Date();
            company.updatedAt = new Date();
            //    fn(null, customer);
           },
           beforeUpdate: function (company, options, fn) {
            company.updatedAt = new Date();
            //    fn(null, company);
           }
       }

    })
    company.associate = function(models) {
            // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
            company.hasMany(models.cuser, { 
                sourceKey: "companyid", foreignKey: "companyid",
                // as:"cusers"
                })
             company.hasMany(models.contract, {sourceKey: 'companyid',foreignKey: 'companyid'})
        // };
      };
    // User.associate =function(models){
    //     User.Many(models.Post)
    // };
    
    return company;
}