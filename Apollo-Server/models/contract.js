

    module.exports = (Sequelize, DataTypes) => {

        const contract = Sequelize.define('contract', {
            contractid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            companyid: DataTypes.INTEGER,
            startdate:DataTypes.STRING,
            enddate:DataTypes.STRING,
            state:{
                type:DataTypes.STRING,
                defaultValue:'Aktif'
            },
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (contract, options, fn) {
                        contract.createdAt = new Date();
                        contract.updatedAt = new Date();
                    },
                    beforeUpdate: function (contract, options, fn) {
                        contract.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            contract.associate = function (models) {
            // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
            contract.hasMany(models.contractrow, {
              sourceKey: 'contractid',
              foreignKey: 'contractid',
            //   as:"companies"
            })
            contract.hasMany(models.contractdetail, {
              sourceKey: 'contractid',
              foreignKey: 'contractid',
                as:"detail"
            })
        };
        //   console.log(user);
        return contract;
    }