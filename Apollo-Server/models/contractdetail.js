

    module.exports = (Sequelize, DataTypes) => {

        const contractdetail = Sequelize.define('contractdetail', {
            contractdetailid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            planid: {
                type: DataTypes.INTEGER,
            },
            contractid: {
                type: DataTypes.INTEGER,
            },
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (contractdetail, options, fn) {
                        contractdetail.createdAt = new Date();
                        contractdetail.updatedAt = new Date();
                    },
                    beforeUpdate: function (contractdetail, options, fn) {
                        contractdetail.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            contractdetail.associate = function (models) {
            // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
            contractdetail.hasOne(models.plan, {
              sourceKey: 'planid',  
              foreignKey: 'planid',
            //   as:"companies"
            })
            // cuser.hasOne(models.user, {
            //   sourceKey: 'userid',
            //   foreignKey: 'userid',
            // //   as:"user"
            // })
        };
        //   console.log(user);
        return contractdetail;
    }