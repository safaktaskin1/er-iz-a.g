

    module.exports = (Sequelize, DataTypes) => {

        const contractrow = Sequelize.define('contractrow', {
            contractrowid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            contractid:  DataTypes.INTEGER,
            productid:  DataTypes.INTEGER,
            count:DataTypes.INTEGER,
            balancecount:DataTypes.INTEGER,
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (contractrow, options, fn) {
                        contractrow.createdAt = new Date();
                        contractrow.updatedAt = new Date();
                    },
                    beforeUpdate: function (contractrow, options, fn) {
                        contractrow.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            contractrow.associate = function (models) {
                contractrow.hasOne(models.product, {sourceKey:'productid',foreignKey: 'productid'})
            // cuser.hasMany(models.company, {
            //   sourceKey: 'companyid',
            //   foreignKey: 'companyid',
            // //   as:"companies"
            // }),
            // cuser.hasOne(models.user, {
            //   sourceKey: 'userid',
            //   foreignKey: 'userid',
            // //   as:"user"
            // })
        };
        //   console.log(user);
        return contractrow;
    }