

    module.exports = (Sequelize, DataTypes) => {

        const cuser = Sequelize.define('cuser', {
            cuserid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            userid: {
                type: DataTypes.INTEGER,
            },
            companyid: {
                type: DataTypes.INTEGER,
            },
            
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (cuser, options, fn) {
                        // console.log("userrrrrrr",user);
                        cuser.createdAt = new Date();
                        cuser.updatedAt = new Date();
                        // user.password = await bcrypt.hashSync(user.password, 10);
                        // bcrypt.hash (user.password,10)
                        //     .then(hash=>{
                        //     user.password=hash;
                        //     next();
                        //     })
                    },
                    beforeUpdate: function (cuser, options, fn) {
                        cuser.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
        cuser.associate = function (models) {
            // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
            cuser.hasMany(models.company, {
              sourceKey: 'companyid',
              foreignKey: 'companyid',
            //   as:"companies"
            }),
            cuser.hasOne(models.user, {
              sourceKey: 'userid',
              foreignKey: 'userid',
            //   as:"user"
            })
        };
        //   console.log(user);
        return cuser;
    }