module.exports = (Sequelize, DataTypes) => {

  const department = Sequelize.define('department', {
    departmentid: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    // parentid: {
    //   type: DataTypes.INTEGER,
    //   allowNull: true,
    // },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },

    updatedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE,
    
  }, {
    hooks: {
      beforeCreate: function (department, options, fn) {
        department.createdAt = new Date();
        department.updatedAt = new Date();
        //    fn(null, category);
      },
      beforeUpdate: function (department, options, fn) {
        department.updatedAt = new Date();
        //    fn(null, category);
      }
    }
  })
  department.associate = function (models) {

    // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
    // company.hasMany(models.user, { sourceKey: 'id', foreignKey: 'companyId' })
    // };
    department.hasMany(models.departmentcategory, {
      sourceKey: "departmentid" ,
      foreignKey: "departmentid",
     // as:"departmentcategories"
    })
    // category.hasMany(models.departmentcategory, {
    //   sourceKey: 'categoryid',
    //   foreignKey: 'categoryid'
    // })
  };
  // User.associate =function(models){
  //     User.Many(models.Post)
  // };
  return department;
}