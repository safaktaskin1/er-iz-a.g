module.exports = (Sequelize, DataTypes) => {

  const departmentcategory = Sequelize.define('departmentcategory', {
    departmentcategoryid: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    // parentid: {
    //   type: DataTypes.INTEGER,
    //   allowNull: true,
    // },
    departmentid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    categoryid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },

    updatedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE
  }, {
    hooks: {
      beforeCreate: function (departmentcategory, options, fn) {
        departmentcategory.createdAt = new Date();
        departmentcategory.updatedAt = new Date();
        //    fn(null, category);
      },
      beforeUpdate: function (departmentcategory, options, fn) {
        departmentcategory.updatedAt = new Date();
        //    fn(null, category);
      }
    }
  })
  departmentcategory.associate = function (models) {

    // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
    // departmentcategory.hasMany(models.department, { sourceKey: 'departmentid', foreignKey: 'departmentid' }) 
    departmentcategory.hasMany(models.category, {
      sourceKey: 'categoryid',
      foreignKey: 'categoryid',
      // as:"departmentcategories"
    })
    departmentcategory.hasOne(models.department,{sourceKey: 'departmentid',foreignKey: 'departmentid'})
  };
  // User.associate =function(models){
  //     User.Many(models.Post)
  // };
  return departmentcategory;
}