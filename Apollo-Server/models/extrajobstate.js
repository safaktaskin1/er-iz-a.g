

    module.exports = (Sequelize, DataTypes) => {

        const extrajobstate = Sequelize.define('extrajobstate', {
            extrajobstateid: { 
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true, 
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
            }, 
            
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (extrajobstate, options, fn) {
                        // console.log("userrrrrrr",user);
                        extrajobstate.createdAt = new Date();
                        extrajobstate.updatedAt = new Date();
                        // user.password = await bcrypt.hashSync(user.password, 10);
                        // bcrypt.hash (user.password,10)
                        //     .then(hash=>{
                        //     user.password=hash;
                        //     next();
                        //     })
                    },
                    beforeUpdate: function (extrajobstate, options, fn) {
                        extrajobstate.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            extrajobstate.associate = function (models) {
            
        };
        //   console.log(user);
        return extrajobstate;
    }