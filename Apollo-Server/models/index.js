
const fs = require('fs')
const path = require("path");
const basename = path.basename(__filename)
const {Sequelize,Op} = require("sequelize");
const env = process.env.mode
const config = require(__dirname + './../config/config.js')[env];

const Models = {};
let sequelize;
//  console.log(process.env[config.username]);
//  return;
if(env=='development'){
    console.log("1");
    sequelize=new Sequelize(
        config.database,
        config.username,
        config.password,
        // config.host,
        // config.dialect,
        // process.env[config.username],
        // process.env[config.password],
        // process.env[config.database],
        
        // process.env[config.port],
        // process.env[config.dialect],
        // process.env[config.port]
        
        config,
        
        );
        // console.log('process.env[config.database]',config.dialect)
    // console.log(sequelize);
}else if(env=='production'){
    sequelize=new Sequelize(
        // config.database,
        // config.username,
        // config.password,
        config);
    console.log('2',);
}

fs.readdirSync(__dirname).filter(file=>{
    
    return (file.indexOf('.') !== 0 ) && (file !== basename) && (file.slice(-3) === '.js')
}).forEach(filem=>{
    // console.log(sequelize.import);
    // const model = sequelize['import'](path.join(__dirname, filem));
    var model = require(path.join(__dirname, filem))(sequelize, Sequelize)
    Models[model.name] = model ;
})

// mysql
Object.keys(Models).forEach(modelName => {
    if (Models[modelName].associate) {
        Models[modelName].associate(Models)
    }
})

Models.sequelize = sequelize
// const Op = sequelize.Op;

module.exports = {
    Models,
    Op,
    Sequelize
};



