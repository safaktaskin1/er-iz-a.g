// const Sequelize = require('sequelize');
// const Op = Sequelize.Op;
module.exports = (Sequelize, DataTypes) => {

  const jobs = Sequelize.define('jobs', {
    jobsid: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
      },
      openuserid: {
        type: DataTypes.INTEGER,
      },
      companyid:{
        type: DataTypes.INTEGER
      },
      // receiveuserid: {
      //   type: DataTypes.STRING,
      // },
      // jobstateid: {
      //   type: DataTypes.INTEGER
      // },
      outjobstate:{
        type:DataTypes.STRING,
        defaultValue:'İşlem Yapılmadı'
      },
      time:{
        type:DataTypes.FLOAT
      },
      amount:{
        type:DataTypes.FLOAT
      },
      buyingamount:{
        type:DataTypes.FLOAT
      },
      categoryid: {
        type: DataTypes.INTEGER
      },
      title:{
        type: DataTypes.STRING
      },
      description:{
        type: DataTypes.STRING
      },

      priority:{
        type: DataTypes.STRING
      },
      note:{
        type: DataTypes.STRING
      },

      updatedAt: {
        type:DataTypes.DATE,
        // field: 'created_at',
      },
      createdAt: {
        type:DataTypes.DATE,
        // field:'updated_at'
      }

  },{ freezeTableName: true },
      {
          hooks: {
              beforeCreate: function (jobs, options, fn) {
                jobs.createdAt = new Date();
                jobs.updatedAt = new Date();
                  //    fn(null, user);
              },
              beforeUpdate: function (jobs, options, fn) {
                jobs.updatedAt = new Date();
                  //    fn(null, user);
              }
          }
      })
      jobs.associate = function (models) {
      jobs.hasOne(models.company, {sourceKey:'companyid',foreignKey: 'companyid',})
      jobs.hasOne(models.user, {sourceKey:'openuserid',foreignKey: 'userid',as:'openuser'})
      jobs.hasOne(models.category, {sourceKey:'categoryid',foreignKey: 'categoryid'})
      // jobs.hasOne(models.user, {sourceKey:'openuserid',foreignKey: 'userid'})
      jobs.hasMany(models.jobstate, {sourceKey:'jobsid',foreignKey: 'jobsid',as:'jobstate'})
      jobs.hasMany(models.jobsrow, {sourceKey:'jobsid',foreignKey: 'jobsid',as:'jobsrow'})
      jobs.hasMany(models.outjob, {sourceKey:'jobsid',foreignKey: 'jobsid'})
      jobs.hasMany(models.upload, {sourceKey:'jobsid',foreignKey: 'jobsid'})
      // user.hasMany(models.company, { sourceKey: 'id', foreignKey: 'userId' })
  };
  //   console.log(user);
  return jobs;
}