module.exports = (Sequelize, DataTypes) => {

  const jobsrow = Sequelize.define('jobsrow', {
    jobsrowid: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    jobsid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    productid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pieceunit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    piece: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    currencyunit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    unitprice: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    currency: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    updatedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE

  }, {
    hooks: {
      beforeCreate: function (jobsrow, options, fn) {
        jobsrow.createdAt = new Date();
        jobsrow.updatedAt = new Date();
        //    fn(null, category);
      },
      beforeUpdate: function (jobsrow, options, fn) {
        jobsrow.updatedAt = new Date();
        //    fn(null, category);
      }
    }
  })
  jobsrow.associate = function (models) {

    jobsrow.hasOne(models.product, {sourceKey:'productid',foreignKey: 'productid',as:'product'})
    
    
    
  // User.associate =function(models){
  //     User.Many(models.Post)
  };
  return jobsrow;
}