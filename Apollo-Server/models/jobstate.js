module.exports = (Sequelize, DataTypes) => {

  const jobstate = Sequelize.define('jobstate', {
    jobstateid: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
      },
      jobsid: {
        type: DataTypes.INTEGER,
      },
      
      state:{
        type: DataTypes.STRING
      },
      userid:{
        type: DataTypes.INTEGER
      },
      transferuserid:{
        type: DataTypes.INTEGER
      },
      transferstateid:{
        type: DataTypes.INTEGER
      },
      active:{
        type:DataTypes.BOOLEAN,
        defaultValue: true
      },
      note:{
        type:DataTypes.STRING
      },
      updatedAt: DataTypes.DATE,
      createdAt: DataTypes.DATE

  },
      {
          hooks: {
              beforeCreate: function (jobstate, options, fn) {
                jobstate.createdAt = new Date();
                jobstate.updatedAt = new Date();
                  //    fn(null, user);
              },
              beforeUpdate: function (jobstate, options, fn) {
                jobstate.updatedAt = new Date();
                  //    fn(null, user);
              }
          }
      })
      jobstate.associate = function (models) {
      
        // jobstate.hasOne(models.jobs, {sourceKey:'jobsid',foreignKey: 'jobsid'})
         jobstate.hasOne(models.user, {sourceKey:'userid',foreignKey: 'userid',as:"stateuser"})
         jobstate.hasOne(models.user, {sourceKey:'transferuserid',foreignKey: 'userid',as:"transferuser"})
         jobstate.hasOne(models.jobs, {sourceKey:'jobsid',foreignKey: 'jobsid',as:'jobsdetail'})
      // user.hasMany(models.company, { sourceKey: 'id', foreignKey: 'userId' })
  };
  //   console.log(user);
  return jobstate;
}