

    module.exports = (Sequelize, DataTypes) => {

        const outjob = Sequelize.define('outjob', {
            outjobid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            userid: {
                type: DataTypes.INTEGER,
            },
            jobsid: {
                type: DataTypes.INTEGER,
            },  
             
            outjobstate: {
                type: DataTypes.STRING,
            },
            companyid: {
                type: DataTypes.INTEGER,
            },
            note:{
                type: DataTypes.STRING
            },
            path:{
                type: DataTypes.STRING
            },
            
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: function (outjob, options, fn) {
                        outjob.createdAt = new Date();
                        outjob.updatedAt = new Date();
                        //    fn(null, category);
                       },
                    beforeUpdate: function (user, options, fn) {
                        outjob.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            outjob.associate = function (models) {
            // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
            // outjob.hasMany(models.company, {
            //   sourceKey: 'companyid',
            //   foreignKey: 'companyid',
            // //   as:"companies"
            // }),
            outjob.hasOne(models.company, {
              sourceKey: 'companyid',
              foreignKey: 'companyid',
            //   as:"user"
            })
            outjob.hasOne(models.user, {
              sourceKey: 'userid',
              foreignKey: 'userid',
            //   as:"user"
            })
            outjob.hasMany(models.upload, {
              sourceKey: 'outjobid',
              foreignKey: 'outjobid',
            //   as:"user"
            })
        };
        //   console.log(user);
        return outjob;
    }