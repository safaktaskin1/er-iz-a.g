

    module.exports = (Sequelize, DataTypes) => {

        const plan = Sequelize.define('plan', {
            planid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
            },
            
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (plan, options, fn) {
                        // console.log("userrrrrrr",user);
                        plan.createdAt = new Date();
                        plan.updatedAt = new Date();
                        // user.password = await bcrypt.hashSync(user.password, 10);
                        // bcrypt.hash (user.password,10)
                        //     .then(hash=>{
                        //     user.password=hash;
                        //     next();
                        //     })
                    },
                    beforeUpdate: function (plan, options, fn) {
                        plan.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            plan.associate = function (models) {
            
        };
        //   console.log(user);
        return plan;
    }