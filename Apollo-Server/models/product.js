module.exports = (Sequelize, DataTypes) => {

  const product = Sequelize.define('product', {
    productid: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    serino: {
      type: DataTypes.STRING,
      allowNull: true
    },
    productcode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    pieceunit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    piece: {
      type: DataTypes.STRING,
      allowNull: true
    },
    buyingprice: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    buyingexchange: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    priceunitmodel: {
      type: DataTypes.STRING,
      defaultValue:'USD',

    },
    companyid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    buyingorderdate:{
      type:DataTypes.STRING,
    },
    ordernumber: {
      type: DataTypes.STRING,
      allowNull: true
    },
    exceptional:{
      type:DataTypes.BOOLEAN
    },
    // path:{
    //   type: DataTypes.STRING
    // },
    // buyingprice:Float,buyingexchange:Float,priceunitmodel:Float
    updatedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE

  }, {
    hooks: {
      beforeCreate: function (product, options, fn) {
        product.createdAt = new Date();
        product.updatedAt = new Date();
        //    fn(null, category);
      },
      beforeUpdate: function (product, options, fn) {
        product.updatedAt = new Date();
        //    fn(null, category);
      }
    }
  })
  product.associate = function (models) {

    product.hasOne(models.company, {sourceKey:'companyid',foreignKey: 'companyid'})
    product.hasOne(models.uploadorder, {sourceKey:'ordernumber',foreignKey: 'ordernumber'})
    // company.hasMany(models.user, { sourceKey: 'id', foreignKey: 'companyId' })
    // };
    // product.hasMany(models.department, {
    //   sourceKey: 'departmentid',
    //   foreignKey: 'departmentid'
    // })
    // category.hasMany(models.departmentcategory, {
    //   sourceKey: 'categoryid',
    //   foreignKey: 'categoryid'
    // })
  };
  // User.associate =function(models){
  //     User.Many(models.Post)
  // };
  return product;
}