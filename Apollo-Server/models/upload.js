

    module.exports = (Sequelize, DataTypes) => {

        const upload = Sequelize.define('upload', {
            uploadid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
            },
            path: {
                type: DataTypes.STRING,
            },
            jobsid: {
                type: DataTypes.INTEGER,
            },
            outjobid: {
                type: DataTypes.INTEGER,
            }, 
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (upload, options, fn) {
                        // console.log("userrrrrrr",user);
                        upload.createdAt = new Date();
                        upload.updatedAt = new Date();
                        // user.password = await bcrypt.hashSync(user.password, 10);
                        // bcrypt.hash (user.password,10)
                        //     .then(hash=>{
                        //     user.password=hash;
                        //     next();
                        //     })
                    },
                    beforeUpdate: function (upload, options, fn) {
                        upload.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            upload.associate = function (models) {
            
        };
        //   console.log(user);
        return upload;
    }