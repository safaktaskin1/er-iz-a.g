

    module.exports = (Sequelize, DataTypes) => {

        const uploadorder = Sequelize.define('uploadorder', {
            uploadorderid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            ordernumber: {
                type: DataTypes.STRING,
            }, 
            path: {
                type: DataTypes.STRING,
            }, 
            updatedAt: DataTypes.DATE,
            createdAt: DataTypes.DATE
        },
            {
                hooks: {
                    beforeCreate: async function (uploadorder, options, fn) {
                        // console.log("userrrrrrr",user);
                        uploadorder.createdAt = new Date();
                        uploadorder.updatedAt = new Date();
                        // user.password = await bcrypt.hashSync(user.password, 10);
                        // bcrypt.hash (user.password,10)
                        //     .then(hash=>{
                        //     user.password=hash;
                        //     next();
                        //     })
                    },
                    beforeUpdate: function (uploadorder, options, fn) {
                        uploadorder.updatedAt = new Date();
                        //    fn(null, user);
                    }
                }
            },
            // { freezeTableName: true}
            )
            uploadorder.associate = function (models) {
            
        };
        //   console.log(user);
        return uploadorder;
    }