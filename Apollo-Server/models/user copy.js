module.exports = (Sequelize, DataTypes) => {

    const user = Sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        companyid: {
            type: DataTypes.INTEGER,
        },
        firstname: {
            type: DataTypes.STRING,
            allowNull: true
        },
        role: {
            type: DataTypes.STRING,
            allowNull: true,
            default: 'user'
        },
        updatedAt: DataTypes.DATE,
        createdAt: DataTypes.DATE

    },
        {
            hooks: {
                beforeCreate: function (user, options, fn) {
                    user.createdAt = new Date();
                    user.updatedAt = new Date();
                    //    fn(null, user);
                },
                beforeUpdate: function (user, options, fn) {
                    user.updatedAt = new Date();
                    //    fn(null, user);
                }
            }
        })
    user.associate = function (models) {
        // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
        // user.hasMany(models.company, { sourceKey: 'id', foreignKey: 'userId' })
    };
    //   console.log(user);
    return user;
}