const bcrypt = require('bcryptjs');
module.exports = (Sequelize, DataTypes) => {

    const user = Sequelize.define('user', {
        userid: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        companyid: {
            type: DataTypes.INTEGER,
        },
        firstname: {
            type: DataTypes.STRING,
            allowNull: true
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: true
        },
        path: {
            type: DataTypes.STRING,
            allowNull: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true
        },
        password: {
            type: DataTypes.STRING,
            default: "$2a$10$Y.sYF9uKxMROpH9kkAOnCum1Y0FfP5K4dfywocBApD.pFFFD7RuCO",

        },
        role: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: 'user' 
        },
        token: {
            type: DataTypes.STRING,
            allowNull: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        
        updatedAt: DataTypes.DATE,
        createdAt: DataTypes.DATE

    },
        {
            hooks: {
                beforeCreate: async function (user, options, fn) {
                    // console.log("userrrrrrr",user);
                    user.createdAt = new Date();
                    user.updatedAt = new Date();
                     user.password = await bcrypt.hashSync(user.password, 10);
                    // bcrypt.hash (user.password,10)
                    //     .then(hash=>{

                    //     user.password=hash;
                    //     next();
                    //     })
                },
                beforeUpdate: function (user, options, fn) {
                    user.updatedAt = new Date();
                    //    fn(null, user);
                }
            }
        },
        // { freezeTableName: true}
        )
    user.associate = function (models) {
        // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
        user.hasOne(models.cuser, {
        //    as:'cusers',
          sourceKey: 'userid',
          foreignKey: 'userid',
          
        })
        user.hasMany(models.userdepartment, {
          sourceKey: 'userid',
          foreignKey: 'userid',
        //   as:"userdepartments"
        })
        
        // User.belongsToMany(models.cuser, {
        //   through: 'UsersWorkingDays',
        //      sourceKey: 'userid',
        //      foreignKey: 'userid',
        // //   as: 'cusers'
        // })
    };
    //   console.log(user);
    return user;
}