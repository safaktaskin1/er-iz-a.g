module.exports = (Sequelize, DataTypes) => {

  const userdepartment = Sequelize.define('userdepartment', {
    userdepartmentid: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    // parentid: {
    //   type: DataTypes.INTEGER,
    //   allowNull: true,
    // },
    userid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    departmentid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },

    updatedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE

  }, {
    hooks: {
      beforeCreate: function (userdepartment, options, fn) {
        userdepartment.createdAt = new Date();
        userdepartment.updatedAt = new Date();
        //    fn(null, category);
      },
      beforeUpdate: function (userdepartment, options, fn) {
        userdepartment.updatedAt = new Date();
        //    fn(null, category);
      }
    }
  })
  userdepartment.associate = function (models) {

    // user.hasOne(models.company, {sourceKey:'id',foreignKey: 'userId'})
    // company.hasMany(models.user, { sourceKey: 'id', foreignKey: 'companyId' })
    // };
    userdepartment.hasMany(models.department, {
      sourceKey: 'departmentid',
      foreignKey: 'departmentid'
    })
    // userdepartment.hasMany(models.departmentcategory,{
    //   sourceKey: 'departmentid',
    //   foreignKey: 'departmentid',
    //   as:'usercategory'
    // })
    // category.hasMany(models.departmentcategory, {
    //   sourceKey: 'categoryid',
    //   foreignKey: 'categoryid'
    // })
  };
  // User.associate =function(models){
  //     User.Many(models.Post)
  // };
  return userdepartment;
}