// const {AuthenticationError,SchemaDirectiveVisitor} = require ('apollo-server-express');
// const {defaultFieldResolver}= require ('graphql');
// var jwt = require('jsonwebtoken');
// class Auth extends SchemaDirectiveVisitor {
//   visitFieldDefinition(field) {
//     const requiredRole = this.args.requires;
//     // console.log("args",requiredRole);
//     const {originalResolve = defaultFieldResolver} = field  ;
//     field.resolve =async function (...args) {
//       let a = false;
//       if ( args[2].headers.authorization) {
//         let token =await args[2].headers.authorization.split(' ')[1]
//         // console.log(token);
//        await jwt.verify(token, process.env.JWT_SECRET,async function (err, decoded) {
//           if(decoded){
//             if (decoded.role=='superuser' ||  decoded.role=='admin' ) {
//               console.log("1");
//               a=true;
//             }else{
//               // console.log("2");
//               a=false;
//             }
//           }else{
//             a=false
//           }
//         }); 
//         }else{
//           a=false
//           console.log("4");
//         }
//       if (a) {
//         return await originalResolve.apply(this, args);
//       }else{
//         // throw new AuthenticationError(`You need following role: `);
//       }
//     }
//   }
// };
// // const Auth="";
// module.exports= Auth;
const { ApolloServer, gql, SchemaDirectiveVisitor } = require("apollo-server");
const { defaultFieldResolver } = require('graphql');
var jwt = require('jsonwebtoken');
class Auth extends SchemaDirectiveVisitor {
  visitObject(type) {
    this.ensureFieldsWrapped(type);
    type._requiredAuthRole = this.args.requires;
  }
  // Visitor methods for nested types like fields and arguments
  // also receive a details object that provides information about
  // the parent and grandparent types.
  visitFieldDefinition(field, details) {
    this.ensureFieldsWrapped(details.objectType);
    field._requiredAuthRole = this.args.requires;
  }

  ensureFieldsWrapped(objectType) {
    // Mark the GraphQLObjectType object to avoid re-wrapping:
    if (objectType._authFieldsWrapped) return;
    objectType._authFieldsWrapped = true;

    const fields = objectType.getFields();

    Object.keys(fields).forEach(fieldName => {
      const field = fields[fieldName];
      const { resolve = defaultFieldResolver } = field;
      field.resolve = async function (...args) {
        // Get the required Role from the field first, falling back
        // to the objectType if no Role is required by the field:
        let a=false;
        const requiredRole =field._requiredAuthRole || objectType._requiredAuthRole;//gelen obje
        if ( args[2].headers.authorization) {
          let token =await args[2].headers.authorization.split(' ')[1]
          // console.log(token);
         await jwt.verify(token, process.env.JWT_SECRET,async function (err, decoded) {
            if(decoded){
              if (decoded.role=='superuser' ||  decoded.role=='admin' || decoded.role==requiredRole) {
              // if (decoded.role==requiredRole) {
                //  console.log("1");
                a=true;
              }else{
                a=false;
                // console.log("2");
              }
            }else{
              a=false
              // console.log("3");
            }
          }); 
          }else{
            a=false
            // console.log("4");
          }
        if (a) {
          return await resolve.apply(this, args);
        }else{
           throw new AuthenticationError(`You need following role: `);
        }
      
      //   if (! requiredRole) {
      //     return resolve.apply(this, args);
      //   }

      //   const context = args[2];
      //   const user = await getUser(context.headers.authToken);
      //   if (! user.hasRole(requiredRole)) {
      //     throw new Error("not authorized");
      //   }

      //   return resolve.apply(this, args);
       };
    });
  }
}
module.exports= Auth;