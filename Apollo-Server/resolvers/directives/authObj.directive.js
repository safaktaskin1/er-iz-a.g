const {AuthenticationError,SchemaDirectiveVisitor} = require ('apollo-server-express');
const {defaultFieldResolver}= require ('graphql');
var jwt = require('jsonwebtoken');
class AuthObj extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const requiredRole = this.args.requires;
    // console.log("args",requiredRole);
    const {originalResolve = defaultFieldResolver} = field  ;
    field.resolve =async function (...args) {
      let a = false;
      if ( args[2].headers.authorization) {
        let token =await args[2].headers.authorization.split(' ')[1]
        // console.log(token);
       await jwt.verify(token, process.env.JWT_SECRET,async function (err, decoded) {
          if(decoded){
            if (decoded.role=='superuser' ||  decoded.role=='admin'  ) {
              // console.log("1");
              a=true;
            }else{
              // console.log("2");
              a=false;
            }
          }else{
            a=false
          }
        }); 
        }else{
          a=false
          // console.log("4");
        }
      if (a) {
        return await originalResolve.apply(this, args);
      }else{
        // throw new AuthenticationError(`You need following role: `);
      }
    }
  }
};
// const Auth="";
module.exports= AuthObj;