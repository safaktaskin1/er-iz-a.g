
const Query = require('./queries/Query');
const category = require('./queries/kategori');
//  import {GraphQLUpload} from 'apollo-server-express'

const Mutation = require('./mutations/index');
const Subscription = require('./subscriptions/index');
module.exports = {
  
  // FileUpload: require("graphql-upload").GraphQLUpload,
  Query,
  Mutation,
  category,   
  // Directives
  Subscription
  // Upload
};
