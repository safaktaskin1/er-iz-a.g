module.exports = {
    // createCompany: async (parent,args, {Models}) => {

    createCompany: async (parent, { name, adress, pbx, fax, cep, email, il, ilce, description, vergiDairesi, vergiNo
    }, { Models,pubsub }) => {
        const model = Models.company

        return await model.create({
            name, adress, pbx, fax, cep, email, il, ilce, description, vergiDairesi, vergiNo
        }).then(data=>{
            pubsub.publish('createSubscribecompany', {
         
                createSubscribecompany: {
                name 
              }
            });
        })
    },
    updateCompany: async (parent, {companyid, name, adress, pbx, fax, cep, email, il, ilce, description, vergiDairesi, vergiNo }, { Models }) => {
        const model = Models.company
        // console.log(companyid, name, adress, pbx, fax, cep, email, il, ilce, description, vergiDairesi, vergiNo);
        return await new Promise((resolve, reject) => {
          model.update({name, adress, pbx, fax, cep, email, il, ilce, description, vergiDairesi, vergiNo}, {where: { companyid },})
          .then(data=>{
  
          })  
          resolve({
            companyid
          })
          })   
  
      },
};