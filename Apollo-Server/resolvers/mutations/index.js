const userMutation = require('./user.mutation');
const companyMutation = require('./company.mutation');
const otherMutation = require('./other.mutation');
const jobsMutation = require('./jobs.mutation');
const productMutation = require('./product.mutation');


//  const photo = require('./photo.mutation');
const Mutation = {
  ...userMutation,
  ...companyMutation,
  ...otherMutation,
  ...jobsMutation,
  ...productMutation
  
  // ...photo
};
module.exports = Mutation;


