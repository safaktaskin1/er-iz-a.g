module.exports = {
    // createCompany: async (parent,args, {Models}) => {

        
    createticket: async (parent, { openuserid ,companyid, categoryid ,priority , title, description, note }, { Models,pubsub }) => {
        const model = Models.jobs
        // console.log(jobsid,state);
        // return false;
        return await model.create({
            openuserid ,companyid, categoryid ,priority ,title, description, note
        }).then(async data => {
            //   console.log("data", data.jobsid);
            
            pubsub.publish('createSubscribejobs', {
         
                createSubscribejobs: {
                jobsid:data.jobsid,
                categoryid
              }
            });
            return data
        });     
    },
    acceptjobs: async (parent, { jobsid, userid, state, categoryid}, { Models, pubsub }) => {
        const model = Models.jobstate
        // console.log("categoryid",categoryid);
        // return false;
        let mevcut = false
        await model.findOne({ where: { jobsid} })
        .then(async jobs=>{
            //  console.log(jobs);
            if(jobs){
                mevcut=true
            // return {res:"Kayıtlı Mail Bulunamadı"}
            }
        })
        // console.log('mevcut',mevcut);
        if (mevcut) {
            return {res:true}
        }else{
            return await model.create({
                jobsid, state,userid
            }).then(async data => {
                pubsub.publish('acceptwaitjobsSubscribe', {
             
                    acceptwaitjobsSubscribe: {
                    jobsid:data.jobsid,
                    categoryid
                  }
                });
                return data
            })
        }
        
    },
    // gerekli
    transferjobs: async (parent, { transferuserid, jobstateid ,jobsid, userid, state, note,transferstateid}, { Models,pubsub,Op }) => {
        const model = Models.jobstate
         console.log("jobstateid",jobstateid);
        // return false;
         return await model.update({active:false},{
             where:
            // {
            //         jobstateid
            //     }
            {  
                [Op.and]:[{
                  'jobsid' : {[Op.eq]:jobsid},
                  'active':{[Op.eq]:1}
                  }]
                }
            }).then(async data=>{
            
            if (data) {
                console.log("var");
                return await model.create({
                    jobsid, state, userid:transferuserid, transferuserid:userid, active:1,note
                })
            }  
  
         }).then(async data =>{
            pubsub.publish('transferSubscribejobs', {
      
                transferSubscribejobs: {
                  transferuserid
                }
              });
         })  
    },
    endjobs: async (parent, { jobstateid ,jobsid, userid, state, note, time}, { Models }) => {
        // return false;
        await Models.jobs.update({time},{where:{jobsid}})
         await Models.jobstate.update({active:false},{where:{jobstateid}}).then(async data=>{

            if (data) {
                return await Models.jobstate.create({
                    jobsid, state, userid, active:1,note
                })
            }

         })
    },
    canceljobs: async (parent, { jobstateid ,jobsid, userid, state, note}, { Models }) => {
        const model = Models.jobstate
         
        // return false;
         await model.update({active:false},{where:{jobstateid}}).then(async data=>{

            if (data) {
                return await model.create({
                    jobsid, state, userid, active:1,note
                })
            }

         })
    },
    // gerekli
    updateJobsnoteandstate: async (parent, { userid, jobsid, jobstateid, note, state}, { Models,pubsub }) => {
        const model = Models.jobstate
         
        // return false;
         await model.update({active:false},{where:{jobstateid}}).then(async data=>{

            if (data) {
                return await model.create({
                    jobsid, state, userid, active:true, note
                }).then(async data => {
                    pubsub.publish('checkedjobsSubscribe', {
                 
                        checkedjobsSubscribe: {
                        jobsid:data.jobsid,
                        openuserid:userid
                      }
                    });
                })
            }
       
         })
    },
    updateJobsnote: async (parent, { jobstateid, note}, { Models }) => {
        const model = Models.jobstate
         
        // return false;
        // return await model.update({note},{where:{jobstateid}})
        return await new Promise((resolve, reject) => {
            model.update({note},{where:{jobstateid}})
            resolve({
                jobstateid,
            })
            })
    },
    updateJobsTime: async (parent, { jobsid, time, userid}, { Models,pubsub }) => {
        const model = Models.jobs
        
        // return await new Promise((resolve, reject) => {
        //     model.update({time},{where:{jobsid}})
        //     resolve({
        //         jobsid,
        //     })
        // })
        return await model.update({time},{
            where:{ jobsid }
           }).then(async data=>{
                if (data) {
                    // pubsub.publish('refreshSubscribejobs', {
      
                    //     refreshSubscribejobs: {
                    //       openuserid:userid
                    //     }
                    //   });
                }
           })
    },
    createOutjob: async (parent, { userid, jobsid, companyid, outjobstate, note}, { Models }) => {
        const model = Models.outjob
        return await model.create({
            userid, jobsid, companyid, outjobstate, note
        }).then(async data => {
            if (data ) {
                return await Models.jobs.update({outjobstate},{where:{jobsid}}).then(async data=>{
                   return {jobsid: jobsid}
  
                })
            }
        });  
    },
    updateOutjob: async (parent, { jobsid ,outjobid, companyid, outjobstate, note}, { Models }) => {
        // console.log(jobsid ,outjobid, companyid, outjobstate, note);
        const model = Models.outjob
        return await model.update({companyid, outjobstate, note},{where:{outjobid}}).then(async data=>{

            if (data ) {
                return await Models.jobs.update({outjobstate},{where:{jobsid}}).then(async data=>{
                   return {outjobid: outjobid}

                })
            }
       
         })
    },
    createextrajobstate: async (parent, { name}, { Models }) => {
        const model = Models.extrajobstate
        return await model.create({
            name
        })
    },
    updateextrajobstate: async (parent, { extrajobstateid,name}, { Models }) => {
        const model = Models.extrajobstate
        return await model.update({name},{
            where:{extrajobstateid}
        }).then(data=>{
            // console.log('data',data);
            return {extrajobstateid,name}
        })
    },
    
};