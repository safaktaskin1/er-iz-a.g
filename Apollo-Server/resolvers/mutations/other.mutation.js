const {parse,join} =require('path')
// const {parse} =require('path')
import {createWriteStream,readFile,readFileSync} from 'fs'
const shortid = require('shortid');
const { promisify } = require('util');
fs = require('fs');
const moment = require('moment')

moment.locale('tr')
// const url = join(__dirname,'../../../uploads/')
const url = process.env.downloadURL
  //  console.log("ddrrdrdrdrdrdrdr",url);
const storeUpload =async ({createReadStream,filename}, jobsid)=>{
  const id =await shortid.generate()
  // console.log("4",filename)
  const {ext}=parse(filename)
  // jobsid + date + random
  const newfilename=jobsid +'_'+ moment().format('DD-MM-YYYY') +'_'+ id + ext
  const name = url+newfilename
  // console.log(name);
  const stream= createReadStream()
  
  // await new Promise((resolve,reject)=>{
    // console.log("name-*-*-*-*-*-*-*-*-*",name);
    await stream 
      .pipe(createWriteStream(name))
      .on("finish",()=>{ 
        console.log("finish");
      })
      .on("error", (error) => {
        console.log("hata");
        // reject()
      })
      return {name, newfilename}
  // })
};
const processUpload = async (upload, jobsid) => {
  // console.log("2",await upload.filename);  
  const { createReadStream, filename, mimetype, encoding } = await upload
  const { name, newfilename } = await storeUpload(upload, jobsid)
  // console.log("3");
  return {name, newfilename};   
}
const avatarstoreUpload =async ({createReadStream,filename}, userid)=>{
  const id =await shortid.generate()
  console.log("4",filename)
  const {ext}=parse(filename)
  // jobsid + date + random
  const newfilename=userid +'_'+ moment().format('DD-MM-YYYY') +'_'+ id + ext
  const name = process.env.avatarURL+newfilename
  console.log(name);
  const stream= createReadStream()

  // await new Promise((resolve,reject)=>{
    console.log("name-*-*-*-*-*-*-*-*-*",name);
    await stream 
      .pipe(createWriteStream(name))
      .on("finish",()=>{ 
        console.log("finish");
      })
      .on("error", (error) => {
        console.log("hata");
        // reject()
      })
      return {name, newfilename}
  // })
};

  
  const avatarprocessUpload = async (upload, userid) => {
    // console.log("2",await upload.filename);  
    const { createReadStream, filename, mimetype, encoding } = await upload
    const { name, newfilename } = await avatarstoreUpload(upload, userid)
    console.log("3");
    return {name, newfilename};   
  }
   
module.exports = {
    singleoutUpload: async (obj, { file,outjobid,name },{Models}) => {
      console.log('outjobid',outjobid);
      console.log('name',name);
      const model=Models.upload
      const upload=await file.file
      
      // console.log("1",upload);
      return await processUpload(upload,outjobid).then(async data=>{
        // console.log("data",data);

        return await model.create({
          path:data.newfilename,
          outjobid,
          name:name
        }).then(dat=>{
          return {filename:upload.filename}
        })
        return dat
      }).catch(function(e) {
        console.error("işşşşşteeeeeeee",e); 
        return {filename:"hata"}
      })

      // console.log("6",id,pathname);
      // return {filename}
    },
    singleUpload: async (obj, { file,jobsid },{Models}) => {
      const model=Models.upload
      const upload=await file.file
      
      console.log("1",upload);
      return await processUpload(upload,jobsid).then(async data=>{
        console.log("data",data);

        return await model.create({
          path:data.newfilename,
          jobsid:jobsid
        }).then(dat=>{
          return {filename:upload.filename}
        })
        return dat
      }).catch(function(e) {
        console.error("işşşşşteeeeeeee",e); 
        return {filename:"hata"}
      })

      // console.log("6",id,pathname);
      // return {filename}
    },
    avatarUpload: async (obj, { file,userid },{Models}) => {
      const model=Models.user
      const upload=await file.file
      
      // console.log("1",upload);
      return await avatarprocessUpload(upload,userid).then(async data=>{
        console.log("data",data);

        return await model.update({
          path:data.newfilename
        },{where:{userid}}).then(dat=>{
          return {filename:upload.filename}
        })
        return dat
      }).catch(function(e) {
        console.error("işşşşşteeeeeeee",e); 
        return {filename:"hata"}
      })

      // console.log("6",id,pathname);
      // return {filename}
    },
    multiUpload: async (obj, { files,jobsid },{Models}) => {
        const model=Models.upload
        const a =await Promise.all( files.map((val)=>{
          return  val
        }))
          return await a.map(async val=>{
            console.log("start val",val);
           const{ name }= await processUpload(val)
            //     // return await model.create({
        //     //   path:name,
        //     //   jobsid:jobsid
        //     // })
      })
        // -*-*-*-*-*-*-
        
        return{jobsid:'3423'}
    },
    createCategory: async (parent, { parentid, name
    }, { Models }) => {
        const model = Models.category
        // console.log(parentid,name);

        return await model.create({
            parentid, name

        })

    },
    deleteCategory:async (parent,{categoryid},{Models,pubsub})=>{
      let model=Models.category
      return await model.destroy({where: { categoryid }})
        .then(data => { 
                    if (data) {
                      // console.log('Deleted successfully',data);
                      return {
                        categoryid: categoryid
                      };
                    }
                  }, function (err) {
                    console.log(err);
                  });
    },  
    updateCategoryName: async (parent, {
            categoryid,
            name
    }, { Models }) => {
        const model = Models.category
        return await new Promise((resolve, reject) => {
              model.update({
                        name
                      }, {
                        where: {
                          categoryid
                        },
              }).then(data=>{

              })
              resolve({
                name,
              })
              })   

    }, 
    updateCategoryParentName: async (parent, {
            categoryid,
            parentid
    }, { Models }) => {
        const model = Models.category
        return await new Promise((resolve, reject) => {
              model.update({
                        parentid
                      }, {
                        where: {
                          categoryid
                        },
              }).then(data=>{

              })
              resolve({
                parentid,
              })
              })

    },
    updateDepartment: async (parent, {
            departmentid,
            name
    }, { Models }) => {
        const model = Models.department
        return await new Promise((resolve, reject) => {
              model.update({
                        name
                      }, {
                        where: {
                          departmentid
                        },
              }).then(data=>{

              })
              resolve({
                name,
              })
              })

    },
    createDepartment: async (parent, {
      name
    }, {
      Models
    }) => {
      const model = Models.department
      // console.log(parentid,name);

      return await model.create({
        name

      })

    },
    createDepartmentUser: async (parent, {
      departmentid,userid
    }, {
      Models
    }) => {
      const model = Models.userdepartment
      // console.log(parentid,name);
      return await model.create({
        departmentid,
         userid
      })
    },
    createDepartmentCategory: async (parent, {
      
      departmentid,
      categoryid
    }, {
      Models
    }) => {
      const model = Models.departmentcategory
      // console.log(parentid,name);

      return await model.create({
        
        departmentid,
        categoryid

      })

    },
    deleteDepartmentUser: async (parent, {userdepartmentid}, {Models,pubsub}) => {
          //  console.log('departmentcategoryid', departmentcategoryid);
          let model = Models.userdepartment
         return await model.destroy({
                where: { userdepartmentid }})
                .then(data => { // rowDeleted will return number of rows deleted
                            if (data) {
                              // console.log('Deleted successfully',data);
                              return {
                                userdepartmentid: userdepartmentid
                              };
                            }
                          }, function (err) {
                            console.log(err);
                          });
    },
    deleteDepartmentCategory: async (parent, {
          departmentcategoryid,
          
        }, {
          Models,
          pubsub
        }) => {
          //  console.log('departmentcategoryid', departmentcategoryid);
          let model = Models.departmentcategory
          
         return await model.destroy({
                where: {
                    departmentcategoryid //this will be your id that you want to delete
                           }
                          }).then(data => { // rowDeleted will return number of rows deleted
                            if (data) {
                              // console.log('Deleted successfully',data);
                              return {
                                departmentcategoryid: departmentcategoryid
                              };
                            }
                          }, function (err) {
                            console.log(err);
                          });
    },
    createcontractrow: async (parent, {contractid, productid, count, balancecount }, { Models }) => {
        const model = Models.contractrow
        return await model.create({
          contractid, productid, count, balancecount
        })

    },
    updatecontractrow: async (parent, {contractrowid, productid, count, balancecount }, { Models }) => {
      const model = Models.contractrow
      return await new Promise((resolve, reject) => {
        model.update({productid, count, balancecount}, {where: { contractrowid },})
        .then(data=>{
 
        })
        resolve({
          contractrowid    
        })
        })   

    },
    deletecontractrow: async (parent, {contractrowid}, {Models,pubsub}) => {
      //  console.log('departmentcategoryid', departmentcategoryid);
      let model = Models.contractrow
     return await model.destroy({
            where: { contractrowid }})
            .then(data => { // rowDeleted will return number of rows deleted
                        if (data) {
                          // console.log('Deleted successfully',data);
                          return {
                            contractrowid: contractrowid
                          };
                        }
                      }, function (err) {
                        console.log(err);
                      });
    },
    createPlan: async (parent, { name }, { Models }) => {
        const model = Models.plan
        return await model.create({
            name
        })

    },
    updatePlan: async (parent, { planid, name }, { Models }) => {
      const model = Models.plan
      return await new Promise((resolve, reject) => {
        model.update({name}, {where: { planid },})
        .then(data=>{
 
        })
        resolve({
          planid,
          name,
        })
        })   

    },
    addcontractplan: async (parent, { plans }, { Models }) => {
      const model = Models.contractdetail
      return await model.bulkCreate(plans).then(async datam => {
        // console.log(data);
        // console.log(datam);
        return datam
      })  

    },
    deleteContractdetail: async (parent, {contractdetailid}, {Models,pubsub}) => {
      //  console.log('departmentcategoryid', departmentcategoryid);
      let model = Models.contractdetail
     return await model.destroy({
            where: { contractdetailid }})
            .then(data => { // rowDeleted will return number of rows deleted
                        if (data) {
                          // console.log('Deleted successfully',data);
                          return {
                            contractdetailid: contractdetailid
                          };
                        }
                      }, function (err) {
                        console.log(err);
                      });
    },
    createContract: async (parent, {companyid, startdate, enddate }, { Models }) => {
      const model = Models.contract
      return await model.create({
        companyid, startdate, enddate
      })

    },
    
    updatecontractrowcount: async (parent, { contractrowid, piece, sum }, { Models }) => {
      // await Models.product.decrement({piece:piece},{where:{
      //   productid
      // }})
      const model = Models.contractrow
      return await new Promise((resolve, reject) => {
        // model.update({balancecount}, {where: { contractrowid },})
        // .then(data=>{
 
        // })
         Models.contractrow.decrement({balancecount:piece},{where:{
          contractrowid
        }})
        resolve({
          contractrowid
        })
        })   

    },
    editupdatecontractrowcount: async (parent, { contractrowid, oldpiece, piece }, { Models }) => {
      // console.log("oldpiece",oldpiece, "piece",piece, "contractrowid",contractrowid);
      return await new Promise((resolve, reject) => {
        // model.update({balancecount}, {where: { contractrowid },})
        // .then(data=>{
 
        // })
        if (oldpiece === piece) {
          console.log("eşit");
        } else if (oldpiece < piece){
          
          let dif= piece - oldpiece
          console.log("küçük",dif);
          Models.contractrow.decrement({balancecount:dif},{where:{
            contractrowid
          }})
          
        } else if (oldpiece > piece){
          
          let dif = oldpiece - piece
          console.log("büyük",dif)
          Models.contractrow.increment({balancecount:dif},{where:{
            contractrowid
          }})
        }
        resolve({
          contractrowid
        })
        })   

    }, 
    updatecontractdate: async (parent, { contractid, startdate, enddate }, { Models }) => {
      return await new Promise((resolve, reject) => {
        const model = Models.contract
        model.update({startdate, enddate}, {where: { contractid },})
        .then(data=>{
          
        })
        resolve({
          contractid    
        })
      })
    },
    deleteuploadfile: async (parent, {path}, { Models, pubsub }) => {
      //  console.log('path', path);
      let model = Models.upload
      const url=process.env.downloadURL + path
    try {

      
      // return await fs.unlinkSync(url).then(async res=>{
        return await model.destroy({
          where: {
              path //this will be your id that you want to delete
                     }
                    }).then(data => { // rowDeleted will return number of rows deleted
                      if (data) {
                        fs.unlinkSync(url)
                        // console.log('Deleted successfully',data);
                        return {
                          path
                        };
                      }
                    }, function (err) {
                      console.log(err);
                    });
      // })
      
      // res.json({deneme:"iştebum!...."})
      // console.log("File is deleted.");
  } catch (error) {
        return{res:"err"}
      }
     
},
    // uploadImage: async (root, { req }) => {
    //   const { filename, mimetype, createReadStream } = await file;
    //   const stream = createReadStream();
    //   // Promisify the stream and store the file, then ...
    //   return true;
    // },
    // multipleUpload: async (root, { files }) => {
    //   const { filename, mimetype, createReadStream, encoding } = await files;
    // },
    
    // Promise.all(files.map(processUpload)),  
    
      // Promise.all(files.map(processUpload)) 
      // let {filename,createReadStream}=await files
      // console.log(filename);
      // console.log(files.ext);
      // let {ext, name}=parse(filename)
      // console.log(name);
      // console.log(files);
    
};