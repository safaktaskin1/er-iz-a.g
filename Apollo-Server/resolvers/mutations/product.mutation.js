
const {parse,join} =require('path')
// const {parse} =require('path')
import {createWriteStream,readFile,readFileSync} from 'fs'
const fs =require ('fs')
const moment = require('moment')
moment.locale('tr')
const url = process.env.downloadURL
  //  console.log("ddrrdrdrdrdrdrdr",url);
const storeUpload =async ({createReadStream,filename}, ordernumber)=>{
  
  // console.log("4",filename)
  
  // jobsid + date + random
  // const name = ordernumber
  const {ext}=parse(filename)
  const name = url + ordernumber + ext
  
  const stream= createReadStream()
  
  // await new Promise((resolve,reject)=>{
    // console.log("name-*-*-*-*-*-*-*-*-*",name);
    await stream 
      .pipe(createWriteStream(name))
      .on("finish",()=>{ 
        console.log("finish");
      })
      .on("error", (error) => {
        console.log("hata");
        // reject()
      })
      return {name}
  // })
};
const processUpload = async (upload, ordernumber) => {
  // console.log("2",await upload.filename);  
  const { createReadStream, filename, mimetype, encoding } = await upload
  const {  name } = await storeUpload(upload, ordernumber)
  // console.log("3");
  return { name};   
}

module.exports={
  // createProduct: async (parent, { name, serino, piece, pieceunit, currency, currencyunit, amount, rate}, { Models }) => {
  createProduct: async (parent, { name, serino,  productcode, pieceunit,buyingprice,buyingexchange,priceunitmodel,companyid, exceptional, ordernumber,piece,buyingorderdate}, { Models,pubsub,Op }) => {
      const model = Models.product
      const productname = await model.findOne({ where: {  name } });
      const productseri = await model.findOne({ where: {
        [Op.and]:[{
          'serino':{
            [Op.eq]:serino
          },
        }]
          
        } });
       if(productseri && exceptional==false){
        return ({res:'Kullanılmış Seri No'})
      }
      else{
        return new Promise(async (resolve, reject) => {
          await model.create({
            name, serino,  productcode, pieceunit, piece, buyingprice,buyingexchange,priceunitmodel,companyid,exceptional,ordernumber,buyingorderdate
        }).then((async product => {
          if (product) {
            //subscribe to
            pubsub.publish('createSubscribeproduct', {
         
              createSubscribeproduct: {
              name
            }
          });
           Object.assign(product, {
             res: 'true'
           });
           return resolve(product)
          }
        }))
         })
      }
  },
  updateProduct: async (parent, { productid, name, serino,  productcode, pieceunit,buyingprice,buyingexchange,priceunitmodel,companyid, exceptional, ordernumber,piece,buyingorderdate}, { Models,Op }) => {
    // console.log(productid, name, serino,  pieceunit,  currencyunit);
      const model = Models.product
      const productname =
      //  false
      await model.findAll({ where: {  
        [Op.and]:[{
          'serino' : {[Op.eq]:serino},
          'productid':{[Op.ne]:productid}
          }]
        } 
      });  
      if(productname.length == 1 && exceptional==false){
        return ({res:'Kullanılmış Seri Numarası'})
      } 
      else{
        return new Promise(async (resolve, reject) => {
          await model.update({
            name, serino,  productcode, pieceunit,buyingprice,buyingexchange,priceunitmodel, companyid, exceptional, ordernumber,piece,buyingorderdate
        },{where:{productid}}).then((async product => {
          if (product) { 

           Object.assign(product, {
             res: 'true'
           });
           return resolve(product)
          }
        }))
         })
      }
  },
  createJobsrow: async (parent, { jobsid, productid, pieceunit, piece, currencyunit, currency, unitprice, rowsamount}, { Models }) => {
    const model = Models.jobsrow
    
    return await model.create({
      jobsid, productid, pieceunit, piece, currencyunit, currency, unitprice, amount:rowsamount,
      }).then(async data=>{
        if (data) {
          // await Models.product.update({piece:0},{where:{
          //   productid
          // }})
          // console.log(pieceunit, piece,);
          if (pieceunit=='SAAT') {
            await Models.product.increment({piece:piece},{where:{
              productid
            }})

            
          }else{
            await Models.product.decrement({piece:piece},{where:{
              productid
            }})
          }
        }
  })
  // [Op.and]:[{
  //   'pieceunit' : {[Op.ne]:'SAAT'},
  //   'productid':{[Op.eq]:productid}
  //   }]                    
  },
  deleteJobsrow:async (parent, { jobsrowid, productid, pieceunit, piece }, { Models }) => {
    let model = Models.jobsrow
          
         return await model.destroy({
                where: {jobsrowid  }
                          }).then(data => { // rowDeleted will return number of rows deleted
                            if (data) {
                              // console.log('Deleted successfully',data);
                              return {
                                jobsrowid
                              };
                            }
                          })
                          .then(async data=>{
                            
                            if (data) {
                              if (pieceunit=='SAAT') {
                                await Models.product.increment({piece:piece},{where:{
                                  productid
                                }})
                              }else{
                                await Models.product.increment({piece:piece},{where:{
                                  productid
                                 
                               }})
                              }
                              
                            }
                          })
  },
  updatejobrow:async (parent, { jobsrowid, jobsid, productid, pieceunit, piece, currencyunit, currency, unitprice, rowsamount }, { Models,Op }) => {
    let model = Models.jobsrow
    await model.update({jobsid, productid, pieceunit, piece, currencyunit, currency, unitprice, amount:rowsamount},{where:{jobsrowid}})  
    .then(async data=>{
      if (data) {
        return jobsid
      }
     })
  },
  updateJobsamount:async (parent, { jobsid, amount,buyingamount }, { Models,Op }) => {
    let model = Models.jobs
    await model.update({ amount, buyingamount },{where:{jobsid}}).then(async data=>{
      if (data) {
        return jobsid
      }
      
      

   })
  },
  buyorderUpload: async (obj, { file,ordernumber },{Models}) => {
    // console.log('ordernumber',ordernumber);
    const model=Models.uploadorder
    const upload=await file.file
    
    console.log("1",upload);
    return await processUpload(upload,ordernumber).then(async data=>{
      console.log("data",data);
      // await Models.uploadorder.create({
      //   path:data.path,
      //   ordernumber:ordernumber
      // })
      return await model.create({
        path:data.name,
        ordernumber:ordernumber
      }).then(dat=>{
        return {ordernumber}
      })
      return dat
    }).catch(function(e) {
      console.error("işşşşşteeeeeeee",e); 
      return {ordernumber:"hata"}
    })

    // console.log("6",id,pathname);
    // return {filename}
  },
}