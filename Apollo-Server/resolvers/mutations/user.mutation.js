const { forgotpasswordsendmail }=require('../../helpers/forgotpasswordmail.js')
const {checkedmail}=require('../../helpers/checkedmail')
const bcrypt = require('bcryptjs')
module.exports = {
    userCreate: async (parent,{firstname,lastname,email, role }, {Models,pubsub}) => {
      const model=Models.user
      
        const user = await model.findOne({ where: { email: email } });
        let password='123456';
      // console.log(user);
      //  if (!foundItem || args.id == undefined) {
        
      //       const item = await model.create({firstname:args.firstname,lastname:args.lastname,email:args.email,role:args.role})
             
            
      //       return  {item, res: true};
      //   }
        
      //   const item = await model.update(newItem, {where});
      //   return {item, res: false};
      
        if(user){
          // console.log("user1",user);
          // throw new Error('Kullanıcı adı kullanımda')
           return ({res:'Kullanıcı adı kullanımda'})
        }else{
          return new Promise(async (resolve, reject) => {
           await model.create({
            firstname, lastname, email, role, password
         }).then((async user => {
           if (user) {

            Object.assign(user, {
              res: 'true'
            });
            pubsub.publish('createSubscribeuser', {
              createSubscribeuser: {
               email
            }
          });
            return resolve(user)
           }
         }))


          })
    }
  
    },
    userEdit: async (parent,{firstname,lastname,email, role }, {Models}) => {
      const model=Models.user
      return await model.update({
            firstname,
            lastname,
            email,
            role
          }, {
        where: {
          email: email
        }
      })
        
  
    },
    createCompanyUser: async (parent, {userid, companyid}, { Models,pubsub}) => {
      const model = Models.cuser
      // console.log(parentid,name);

      return await model.create({
        userid,
        companyid
      }).then(data=>{
        // createsubscribecompanyuser
        pubsub.publish('createsubscribecompanyuser', {
          createsubscribecompanyuser: {
           userid
        }
      });
      })

    },
    changePassword:async (root,{ email, password, token },{Models}) => {
      // console.log(usermail, tok  en );
      const model = Models.user
      const newpassword = await bcrypt.hashSync(password, 10);
      return await model.findOne({where:{token}}).then(async user=>{
        if(user){
          return await model.update({password:newpassword},{where: {token} }).then(data=>{
            return {res:'true'}
          });
        }
      })
    },
    sendForgotPasswordMail:async (root,{ email, token },{Models}) => {
      const model = Models.user
      await forgotpasswordsendmail({ email, token })
      return await model.update({token},{where: {email} });
    },
    checkedmail:async (root,{ jobsid,usermail },{Models}) => {
      
      await checkedmail({ jobsid, usermail })
      return jobsid
      
    },
};