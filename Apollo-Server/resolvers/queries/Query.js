//  const company = require("../../models/company.js")
// import { forgotpasswordsendmail } from '../../helpers/forgotpasswordmail' ;
const bcrypt = require('bcryptjs');
// const { Promise } = require('sequelize');
const tokens = require('../../config/token')
const forgottokens = require('../../config/tokenforgot')
const { forgotpasswordsendmail }=require('../../helpers/forgotpasswordmail.js')
 const {sendForgotPasswordMail}=require  ('../mutations/user.mutation')
 const moment = require('moment')
moment.locale('tr')

// const sequelize = require('Sequelize');
// const {Op} =require('../../models/index')
// const {Op} = require("sequelize");
//
module.exports = {
 
    allUser: async (parent, args ,{ Models }) => {
      // (parent, args, context, info)
      // console.log(args)
        const Model = Models.user
        // console.log(Model);
      return await Model.findAll(
        {
          // attributes: ['company.name'],
        include: [
          {
            model: Models.cuser,
            // as:'cusers',
            include: [
                {
                  model: Models.company,
                  // as:'companies',
                },
              ]

          },

        ]
      })
    },
    userdetail: async (parent, {email}, { Models}) => {
      const Model = Models.user
      console.log(moment().format('DD-MM-YYYY HH:mm'),email);
      return await Model.findOne({
        where:{email:email},
        // attributes: ['company.name'],
        // include: [
        //   {
        //     model: Models.user,
        //     //  model: Models.cuser,

        //   }
        // ]

          include: [

              {
                model: Models.cuser,
                // as:'cusers',
                include: [
                    {
                      model: Models.company,
                      // as:'companies',
                    },
                  ]

              },
              {
                model:Models.userdepartment,
                include: [
                  {
                    model: Models.department,
                    include:[
                      {
                        model: Models.departmentcategory
                      }
                    ]
                  },
                //   // {
                //   //   model: Models.user,
                //   // },
                ]
              }
            ]

      })
    },
    allCuser: async (parent, _ ,{ Models }) => {
        const Model = Models.cuser
      return await Model.findAll()
    },
    loginUser: async ( _ , {email, password } ,{ Models }) => {

      const Model = Models.user
      return new Promise(async (resolve, reject) => {
      await Model.findOne({ where: { email} })
      .then((async user=>{
        //  console.log(user);
        if(!user){
          // console.log("Kayıtlı Mail Bulunamadı");
          let res={res:"Kayıtlı Mail Bulunamadı"}
              return resolve(res)
          // Kayıtlı Mail Bulunamadı
        }else{
          bcrypt.compare(password, user.password).then(async function(hash) {

            if(hash){

              // console.log("Kayıtlı Mail Bulundu");
              //bura
              var token = tokens.generate({firstname:user.firstname,lastname:user.lastname,email:user.email,role:user.role})

               Object.assign(user, {res:'true', token:token});
              // console.log(user.dataValues)
               return resolve(user)
            }else{
              // console.log("Şifre yanlış")
              let res={res:"Şifre yanlış"}
              return resolve(res)
              //return ({_id:"",username:"",lastname:"",usermail:"",res:'Şifre yanlış'})
            }


          })
        }
      }))
    })
    },
    companiesQuery: async (parent, _ ,{ Models }) => {
        const Model = Models.company
        // console.log(Model);
      return await Model.findAll(
        {
          // attributes: ['company.name'],
        // include: [
        //   {
        //     model: Models.user,
        //     //  model: Models.cuser,

        //   }
        // ]
        include: [
          {
            model: Models.cuser,
            include: {model: Models.user},
          },
          {
          model:Models.contract,
          include: [
          {
            model: Models.contractrow,
            include: {model: Models.product}
          },
          {
            model:Models.contractdetail,as:"detail",
            include:{model: Models.plan}
          }
        ],
        }
      ],
      order:[
        ['name', 'ASC'],
        [  { model: Models.contract}, 'createdAt', 'DESC' ],
      ]
      })
    },
    categoryQuery: async(parent, _ , {Models})=>{
      const Model = Models.category
      return await Model.findAll({
        // include:[
        //   // {
        //   //   model:Models.outjob,
        //   // },
        //   {
        //     model:Models.category,as:"parent"
        //   },
        // ]
      });
    },
    allCategory: async (parent, _, {Models,req }) => {
          // console.log(req.headers);
      const Model = Models.category
      return await Model.findAll();
    },
    departmentQuery: async (parent, _, { Models }) => {
      const Model = Models.department
      // console.log(Model);
      return await Model.findAll({
        // attributes: ['company.name'],
        // include: [
        //   {
        //     model: Models.user,
        //     //  model: Models.cuser,

        //   }
        // ]
        include: [{
          model: Models.departmentcategory,
          include: [{
                model: Models.category,

              }]
        }]
      })
    },
    departmentFilterQuery: async (parent, {userid}, {Models}) => {
      const Model = Models.userdepartment
      //  console.log(userid);
      return await Model.findAll({
        where:{userid:userid},
        // attributes: ['company.name'],
        // include: [
        //   {
        //     model: Models.user,
        //     //  model: Models.cuser,

        //   }
        // ]

          include: [{
                model: Models.department,

              }]

      })
    },
    departmentcategoryQuery: async (parent, {departmentid}, {
      Models
    }) => {
      const Model = Models.department
      // console.log(Model);
      return await Model.findAll({
            where: {
              departmentid: departmentid
            },
        include:[
          {
            model: Models.departmentcategory,

            include: [{
              model: Models.category,
              // group: ['departmentcategories.categories.name'],
            }],

          }
        ],
        // group: ['departmentcategories.categories'],
      })
    },
    alljobsQuery: async (parent, _, {Models,Op,Sequelize}) => {
      const Model = Models.jobs
      // console.log(Model);
      return await Model.findAll({
        where: {
              [Op.or]:[{
                [Op.and]:[
                  {
                    '$jobstate.state$':{
                      [Op.and]:
                        [
                          {[Op.ne]:'Onaylandı'},
                          {[Op.ne]:'İptal'},
                        ]
                    },
                  },
                  {
                    '$jobstate.active$':{[Op.eq]:"1"}
                  },
                ],
            },
            {
              '$jobstate.state$':{[Op.is]:null} 
            }
          ]
        },
          
        include:[
          // {
          //   model:Models.outjob,
          // },
          {
            model:Models.user,as:"openuser"
          },
          {
            model:Models.upload,
          },
          {
            model:Models.company,
            include:[
              {
                model:Models.contract,
                include:[
                  {
                    model:Models.contractrow
                  }
                ]
              }
            ]
          },
          {
            model:Models.outjob,
            include:[
              { model:Models.company },
              {model:Models.user},
              { model:Models.upload }
            ],
            // order: [
            //   [{model:Models.outjob},'createdAt', 'ASC'],
            // ],
          },
          {
            model:Models.jobsrow,as:"jobsrow",
              include:[
                {
                  model:Models.product,as:"product"
                },
                
              ]
          },
          {
            model: Models.jobstate, as:"jobstate",
                // attributes: [],
                // required: true,
                // right: true,
                // where: { 'state': { [Op.ne]: 'Onaylandı' } },
              include:[
                {
                model:Models.jobs,as:"jobsdetail",
              },
                {
                model:Models.user,as:"stateuser",
              },
              {
                model:Models.user,as:"transferuser"
              },
              ],
          },
          {
            model: Models.category,
              include:{
                model:Models.departmentcategory,
                  include:[{
                    model:Models.department,
                  }]
              }
          },
        ],
        order: [
          ['jobsid', 'DESC'],
          [  { model: Models.outjob }, 'createdAt', 'ASC' ],
          [  { model: Models.jobsrow }, 'createdAt', 'ASC' ],
          // [  { model: Models.jobstate }, 'jobstateid', 'ASC' ],
          // ['jobsid', 'DESC'],
          
        ],
        // distinct: true
      },

      )
    }, 
    allcancelQuery:async(parent,_,{Models,Op})=>{
      const Model=Models.job
      return await Model.findAll({
        where: {
              
                [Op.and]:[
                  {
                    '$jobstate.state$':
                          {[Op.ne]:'İptal'},
                  },
                  {
                    '$jobstate.active$':{[Op.eq]:"1"}
                  },
                ],
            },
            
        
          
        include:[
          // {
          //   model:Models.outjob,
          // },
          {
            model:Models.user,as:"openuser"
          },
          {
            model:Models.upload,
          },
          {
            model:Models.company,
            include:[
              {
                model:Models.contract,
                include:[
                  {
                    model:Models.contractrow
                  }
                ]
              }
            ]
          },
          {
            model:Models.outjob,
            include:[
              { model:Models.company },
              {model:Models.user},
              { model:Models.upload }
            ],
            // order: [
            //   [{model:Models.outjob},'createdAt', 'ASC'],
            // ],
          },
          {
            model:Models.jobsrow,as:"jobsrow",
              include:[
                {
                  model:Models.product,as:"product"
                },

              ]
          },
          {
            model: Models.jobstate, as:"jobstate",
                // attributes: [],
                // required: true,
                // right: true,
                // where: { 'state': { [Op.ne]: 'Onaylandı' } },
              include:[{
                model:Models.user,as:"stateuser",
              },
              {
                model:Models.user,as:"transferuser"
              },
              ],
          },
          {
            model: Models.category,
              include:{
                model:Models.departmentcategory,
                  include:[{
                    model:Models.department,
                  }]
              }
          },
        ],
        order: [
          ['jobsid', 'DESC'],
          [  { model: Models.outjob }, 'createdAt', 'ASC' ],
          [  { model: Models.jobsrow }, 'createdAt', 'ASC' ],
          // [  { model: Models.jobstate }, 'jobstateid', 'ASC' ],
          // ['jobsid', 'DESC'],
          
        ],
        // distinct: true
      })
    },
    departmentjobsQuery: async (root, {list}, {Models,Op}) => {
      // console.log(list);
      const Model = Models.jobs
      let arr=[]
      for(var i = 0; i < list.length; i++){
        // console.log(list[i].departmentid);
        arr.push(list[i].departmentid)
    }
    
      return await Model.findAll({
        where: {
          [Op.and]:[
            {
              '$jobstate.state$':{
                [Op.and]:[
                {[Op.ne]:'Onaylandı'},
                {[Op.ne]:'İptal'},
                ]
              },
            },
            {
                '$jobstate.active$':{[Op.eq]:"1"}
            },
            {
              '$category.departmentcategory.department.departmentid$' : {[Op.in]:arr},
          }
        ]
        },
        include:[
          {
            model:Models.user,as:"openuser"
          },
          {
            model:Models.upload,
          },
          {
            model:Models.company,
            include:[
              {
                model:Models.contract,
                include:[
                  {
                    model:Models.contractrow
                  }
                ]
              }
            ]
          },
          {
            model:Models.outjob,
            include:[
              {model:Models.company},
              {model:Models.user},
              { model:Models.upload }
            ],
            // order: [
            //   [{model:Models.outjob},'createdAt', 'ASC'],
            // ],
          },
          {
            model:Models.jobsrow,as:"jobsrow",
            include:[{
              model:Models.product,as:"product"
            }]
          },
          {
            model: Models.jobstate,as:"jobstate",
              include:[{
                model:Models.user,as:"stateuser"
              },
              {
                model:Models.user,as:"transferuser"
              },
              ],
          },
          {
            model: Models.category,
            include:{
              model:Models.departmentcategory,
              include:[{
                model:Models.department,
              }]
            }
          },
        ],
        
        order: [
          ['createdAt', 'DESC'],
          [  { model: Models.outjob }, 'createdAt', 'ASC' ],
          [  { model: Models.jobsrow }, 'createdAt', 'ASC' ],
          
          // ['created_at', 'asc']
        ],
        // distinct: true
      })
    },
    endjobsQuery: async (parent, _, {Models,Op}) => {
      const Model = Models.jobs
      // return 
      return await Model.findAll({
        where: {
          
                  // [Op.and]:[
                  //   {
                  //     '$jobstate.state$':{[Op.eq]:['Onaylandı']},
                  //   },
                  //   {
                  //     '$jobstate.active$':{[Op.eq]:'1'}
                  //   },
                  // ],
                  [Op.and]:[
                    {
                      '$jobstate.state$':{[Op.or]:[
                            {[Op.eq]:'Onaylandı'},
                            {[Op.eq]:'İptal'},
                          ]
                      },
                    },
                    {
                      '$jobstate.active$':{[Op.eq]:"1"}
                    },
                  ],
              },
        
        include:[
          
          
          {
            model:Models.user,as:"openuser"
          },
          {
            model:Models.upload,
          },
          {
            model:Models.company,
            include:[
              {
                model:Models.contract,
                include:[
                  {
                    model:Models.contractrow
                  }
                ]
              }
            ]
          },
          {
            model:Models.outjob,
            include:[
              { model:Models.company },
              {model:Models.user},
              { model:Models.upload }
            ],
            // order: [
            //   [{model:Models.outjob},'createdAt', 'ASC'],
            // ],
          },
          {
            model:Models.jobsrow,as:"jobsrow",
              include:[
                {
                  model:Models.product,as:"product"
                }
              ]
          },
          {
            model: Models.jobstate, as:"jobstate",
            
            // where: {

              
                  
            //         [Op.and]:[
            //           {
            //           'state':{[Op.eq]:['Onaylandı']},
            //           },
            //           {
            //             'active':{[Op.eq]:'1'}
            //           },
            //         ],
                  
            //       // [Op.contians]:{state:'Onaylandı'},
                  
                    
                  
            // },
            //  raw:true,
              
              include:[{
                model:Models.user,as:"stateuser",
              },
              {
                model:Models.user,as:"transferuser"
              },

              {
                model:Models.jobs,as:"jobsdetail"
              },
              ],
          },
          {
            model: Models.category,
              include:{
                model:Models.departmentcategory,
                  include:[{
                    model:Models.department,
                  }]
              }
          },
        ],
        order: [
          // ['createdAt', 'ASC'],
          [  { model: Models.jobstate }, 'createdAt', 'DESC' ], 
          [  { model: Models.outjob }, 'createdAt', 'ASC' ],
          [  { model: Models.jobsrow }, 'createdAt', 'ASC' ],
          
          // ['jobsid', 'DESC'],
          
        ],
        // raw : true , // <----------- Magic is here
        // nest : true // <----------- Magic is here
        // distinct: true
      })
      // .then(res=>{
      //   var jsonString = JSON.stringify(res); 
      //   var obj = JSON.parse(jsonString);
      //   var list= []
      //   list= obj.filter((element) => 
      //     element.jobstate.some((t) =>t.state == 'Onaylandı'  &&  t.active))
      //     .map( element => {
      //       const newElt = Object.assign({}, element); 
      //       return newElt
      //     });
      //     // console.log('list',list)
          
      //     return list
      // })
    },
    departmenttransferjobsQuery: async (root, {userid}, {Models,Op}) => {
      // console.log(list);
      const Model = Models.jobs

    // console.log(arr);
      return await Model.findAll({


        include:[
          {
            model:Models.user,as:"openuser"
          },
          {
            model:Models.company,
          },
          {
            model:Models.jobsrow,as:"jobsrow",
            include:[{
              model:Models.product,as:"product"
            }]
          },
          {
            model: Models.jobstate,as:"jobstate",
              include:[{
                model:Models.user,as:"stateuser"
              },
              {
                model:Models.user,as:"transferuser"
              },
              ],
          },
          {
            model: Models.category,
            include:{
              model:Models.departmentcategory,
              include:[{
                model:Models.department,
              }]
            }
          },
        ],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
    },
    allProducts: async (parent, args, {Models})=>{
      // console.log(Models);
      const model = Models.product
      return await model.findAll({
        include:[
          // {
          //   model:Models.outjob,
          // },
          {
            model:Models.company
          },
          {
            model:Models.uploadorder
          },
        ],
        order: [
          ['name', 'ASC'],
          
        ],
      })
    },
    // filterProducts: async (parent, args, {Models})=>{
    //   // console.log(Models);
    //   const model = Models.product
    //   return await model.findAll({
    //     where: {
              
    //       piece: {
    //         [Op.ne]: 0
    //       }
    //   },
    //     order: [
    //       ['name', 'ASC'],
    //     ],
    //   })
    // },
    contractQuery:   async (parent, {companyid}, { Models }) => {
      const Model = Models.contract
      //  console.log(userid);
      return await Model.findAll({
        where:{companyid:companyid},
          // include: [{
          //       model: Models.department,

          //     }]

      })
    },
    planQuery: async (parent, _ ,{ Models }) => {
      const Model = Models.plan
      return await Model.findAll({
        order: [
          ['name', 'ASC'],
        ],
      })
    },
    userjoblist: async (parent, {openuserid}, {Models}) => {
      const Model = Models.jobs
      // console.log(Model);
      return await Model.findAll({
        where:{openuserid},
        include:[
          // {
          //   model:Models.outjob,
          // },
          {
            model:Models.user,as:"openuser"
          },
          {
            model:Models.upload,
          },
          {
            model:Models.company,

          },
          {
            model:Models.jobsrow,as:"jobsrow",
              include:[
                {
                  model:Models.product,as:"product"
                },

              ]
          },
          {
            model: Models.jobstate, as:"jobstate",
              include:[{
                model:Models.user,as:"stateuser"
              },
              {
                model:Models.user,as:"transferuser"
              },
              ],
          },
        ],
        order: [
          [  { model: Models.jobstate }, 'jobstateid', 'ASC' ],
          ['createdAt', 'DESC'],
        ],
      },

      )
    },
    forgotUserQuery:async (parent, {email}, {Models})=> {
      const model = Models.user
      // console.log(moment().format('DD-MM-YYYY HH:mm'),email);
      //  let ibo= await model.find({usermail:usermail})
       return await new Promise(async (resolve,object) =>{
         await model.findOne({where:{ email}})
        .then((async user=>{
          // console.log(user);
          if(!user){
            return resolve({res:false})
          }else{
              var token =await  forgottokens.generate({email})
              // await forgotpasswordsendmail({ email, token })
              // await model.update({token},{where: {email} });
              await forgotpasswordsendmail({ email, token }).then(async mail=>{
                // console.log('mailquery',mail);
                if (mail) {
                  console.log('mail--ok',);

                    await model.update({token},{where: {email} }).then(data=>{
                      console.log('data',data);
                      resolve({res:true})
                    })

                }
              })


              // await sendForgotPasswordMail(email,token).then(data=>{
              //   if (data) {
              //     return resolve({res:true})
              //   }else{
              //     return resolve({res:false})
              //   }
              // })

      }
        })
        )
      })
    },
    extrajobstate: async (parent, _ ,{ Models }) => {
      const Model = Models.extrajobstate
    return await Model.findAll()
    },
    ordersearch: async (parent, {ordernumber} ,{ Models }) => {
      const Model = Models.uploadorder
      return await Model.findAll({
        where:{ordernumber},
      })
    },
    changeversion:async(parent,_,{Models,pubsub})=>{
      pubsub.publish('changeversionSubscribe', {
         
        changeversionSubscribe: {}
    });
    },
    
    
}




