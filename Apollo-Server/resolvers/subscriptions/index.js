const other = require('./other.subscription');
const jobs = require('./jobs.subscription');
const Subscription = {
  ...jobs,
  ...other
  
};
module.exports = Subscription;