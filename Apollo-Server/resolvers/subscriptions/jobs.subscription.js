module.exports={
  createSubscribejobs: {
    subscribe:(parent,args,{pubsub}) => pubsub.asyncIterator('createSubscribejobs'),
    // subscribe:(parent,args,{pubsub})=> pubsub.asyncIterator('transferSubscribejobs')
  },
  transferSubscribejobs:{
    subscribe:(parent,args,{pubsub})=> pubsub.asyncIterator('transferSubscribejobs')
  },
  acceptwaitjobsSubscribe:{
    subscribe:(parent,args,{pubsub})=> pubsub.asyncIterator('acceptwaitjobsSubscribe')
  },
  checkedjobsSubscribe:{
    subscribe:(parent,args,{pubsub})=> pubsub.asyncIterator('checkedjobsSubscribe')
  },
  refreshSubscribejobs:{
    subscribe:(parent,args,{pubsub})=> pubsub.asyncIterator('refreshSubscribejobs')
  },
  changeversionSubscribe:{
    subscribe:(parent,args,{pubsub})=> pubsub.asyncIterator('changeversionSubscribe')
  },
}