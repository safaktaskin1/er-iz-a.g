'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
      await queryInterface.bulkInsert('user', [{
        firstname: 'admin',
        lastname:'admin',
        email:'a@a.com',
        password:'123456',
        role:'superuser',
        createdAt:new Date(),
        updatedAt:new Date(),
      }], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
      await queryInterface.bulkDelete('People', null, {});
     */
  }
};
