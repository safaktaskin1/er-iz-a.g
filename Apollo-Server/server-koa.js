import express from 'express';
const app = express();
const http =require('http');
const https =require('https');
var cors = require('cors')
// import https from 'https'
require('dotenv').config();
const { ApolloServer, gql, PubSub, AuthenticationError,GraphQLUpload } = require('apollo-server-express');
const { GraphQLObjectType ,GraphQLSchema} = require ("graphql");
// import {} from "graphql"
const {Auth, AuthObj} = require('./resolvers/directives');
const { importSchema } = require('graphql-import');
const {bodyParserGraphQL}= require('body-parser-graphql')
const { makeExecutableSchema } = require('@graphql-tools/schema');
const { stitchSchemas }= require('@graphql-tools/stitch');
const { graphqlUploadExpress,processRequest } = require('graphql-upload');
const {createWriteStream} = require('fs');
const path = require("path");
const fs =require ('fs');
// const bodyParser = require('body-parser');
const logger = require('./config/logger');
var jwt = require('jsonwebtoken');
const fetch = require('node-fetch');
const moment = require('moment')
moment.locale('tr')
process.on('uncaughtException', (err) => {
  console.log("-*-*-*-*-*-*-*-*-*-");
  
});
app.use(cors())

require('./routes/router')(app);



// -*---**-*-*--*-*-*-*

// console.log(schemaUploadr);
// -*-*-*-*-*-*-*-*-*-*-
const {success,info,error}=require('consola');
// require('dotenv').config();

// const upresolvers = {
//   FileUpload: GraphQLUpload,
  
// }
const resolvers = require('./resolvers/index');
const {Models,Op} =require('./models/index');
const { response } = require('express');
const pubsub=new(PubSub);
const basicDefs = importSchema('./schema.graphql');

const configurations = {
  production: { ssl: false, port: 4000, hostname: 'localhost' },
  // production: { ssl: false, port: 4000, hostname: 'localhost' },
  development: { ssl: false, port: 4000, hostname: 'localhost' }
}
// console.log(process.env.NODE_ENV);
const environment = process.env.NODE_ENV 
const config = configurations[environment]
//  console.log(resolvers);
// resolvers.FileUpload = GraphQLUpload;
const apollo = new ApolloServer({
  // introspection: false,

    typeDefs: [basicDefs],
    resolvers: [resolvers],
    schemaDirectives: {
      auth: Auth,
      authObj:AuthObj,
    },
    formatError: (err) => {
      console.error(err);
      return err;
    },
    // introspection: true,
    playground: true,
    

    // validationRules: [NoIntrospection],
  context: async ({ req, payload }) => {
      // console.log(req.headers.authorization);
    // let a = false;
    //  if ( req.headers.authorization) {
    //    let token = req.headers.authorization.split(' ')[1]
      
      
    //    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {

    //      if(decoded){
   
    //      }
     //    });
      
      
      return {
        ...req,
        pubsub,
        Models,
        Op
        // SchemaDirectiveVisitor
      // };
     }
    
  },

  uploads: false,
  graphiql: true,
  debug: true,

 });
  app.set('view engine', 'ejs');
  app.use(express.static(path.join(__dirname, './helpers')))
  app.use('/graphql',graphqlUploadExpress({ maxFileSize: 250000000, maxFiles: 10 }))
  // processRequest(response=>{                          10000000
  //   console.log("-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*response");
  // })
// graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
apollo.applyMiddleware({

  app,
  cors: {
    origin: '*',
    credentials: true,
    // methods: ['PUT', 'POST', 'GET', 'DELETE', 'OPTIONS'],
    // allowedHeaders: ['Content-Type', 'Content-Length', 'Authorization', 'Accept', 'X-Requested-With', 'x-access-token','multipart/form-data','application/json']
  },

});
let server
if (config.ssl) {
  server = https.createServer(
    {
        key: fs.readFileSync(`/etc/apache2/ssl/private/www.emosetekstil.com.tr.key`),
        cert: fs.readFileSync(`/etc/apache2/ssl/private/www.emosetekstil.com.tr.crt`)
    },
    app  
  )
} else {
  server = http.createServer(app)
}
// Süreli Görev
setInterval(async() => {
      const contract= await Models.contract.findAll();
      JSON.stringify(contract);
      contract.forEach(async val=>{
        let end =  moment(val.enddate,'DD-MM-YYYY').toDate()
        const dif= moment().diff(end)
        console.log(dif);
        if (val.state=='Aktif' && dif > 0) {
           await Models.contract.update({state:'Pasif'},{where:{contractid:val.contractid}}).then(async data=>{
            console.log("başarılı");
          })
        }
      })
      
}, 86400000);

apollo.installSubscriptionHandlers(server);

const startApp=async()=>{
  try {
server.listen({ port: config.port }, (error) =>{
  // console.log(`🚀 Server ready at http${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.graphqlPath}`);
  // console.log(`🚀 Server Subscriptions at ws${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.subscriptionsPath}`);
  console.log(`🚀 Server Time at ${moment().format('DD-MM-YYYY HH:mm')} `);
})
} catch (err) {
  console.log("rrrrrrrrrr-*-*-*----*");
  error({
    badge: true,
    message: err.message
  });
}
}
startApp();
