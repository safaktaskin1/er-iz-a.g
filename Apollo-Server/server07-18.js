const express = require('express');
const app = express();
const http =require('http');
const https =require('https');
var cors = require('cors')
// import https from 'https'
require('dotenv').config();
const { ApolloServer, gql, PubSub, AuthenticationError, GraphQLUpload } = require('apollo-server-express');

const { GraphQLObjectType ,GraphQLSchema} = require ("graphql");
// import {} from "graphql"
const {Auth, AuthObj} = require('./resolvers/directives');
const { importSchema } = require('graphql-import');

// const { FileUpload, graphqlUploadExpress } =require ('graphql-upload')

const {createWriteStream} = require('fs');
// const mongoose = require("mongoose"); 1
const path = require("path");
const fs =require ('fs');
const bodyParser = require('body-parser');
const logger = require('./config/logger');
var jwt = require('jsonwebtoken');
const fetch = require('node-fetch');
const moment = require('moment')
moment.locale('tr')
 
app.use(cors())
require('./routes/router')(app);
// const { parseString } = require('xml2js')
// const parseXML = require('util').promisify(parseString)


const {success,info,error}=require('consola');
// require('dotenv').config();


const resolvers = require('./resolvers/index');
// const upload={FileUpload:GraphQLUpload};
// console.log(resolver.Query);
// const FileUpload= {FileUpload:GraphQLUpload};
// const resolvers=resolver.concat(FileUpload)
// const resolvers=Object.assign(resolver,upload)
// console.log(resolvers);
const {Models,Op} =require('./models/index');
const pubsub=new(PubSub);
const basicDefs = importSchema('./schema.graphql');
// const basicDefs = a.replace(
//   "scalar Upload",
//   ""
// )
// console.log(basicDefs);
const configurations = {
  production: { ssl: false, port: 4000, hostname: 'localhost' },
  // production: { ssl: false, port: 4000, hostname: 'localhost' },
  development: { ssl: false, port: 4000, hostname: 'localhost' }
}
console.log(process.env.NODE_ENV);
const environment = process.env.NODE_ENV 
const config = configurations[environment]
//  console.log(resolvers);
// resolvers.FileUpload = GraphQLUpload;
const apollo = new ApolloServer({
  // introspection: false,

    typeDefs: [basicDefs],
    resolvers: [resolvers],
    schemaDirectives: {
      auth: Auth,
      authObj:AuthObj,
    },
    formatError: (err) => {
      console.error(err);
      return err;
    },
    introspection: true,
    playground: true,
     

    // validationRules: [NoIntrospection],
  context: async ({ req, payload }) => {
      // console.log(req.headers.authorization);
    // let a = false;
    //  if ( req.headers.authorization) {
    //    let token = req.headers.authorization.split(' ')[1]
      
      
    //    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {

    //      if(decoded){
   
    //      }
     //    });
      
      
      return {
        ...req,
        pubsub,
        Models,
        Op
        // SchemaDirectiveVisitor
      // };
     }
    
  },
  uploads: false,
  graphiql: true,
  debug: true,

 });

// console.log(apollo.Upload);
// app.use(graphqlUploadExpress({ maxFileSize: 10000, maxFiles: 10 }));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, './helpers')))

apollo.applyMiddleware({

  app,
  cors: {
    credentials: true,
    origin: true
  },
});
let server
if (config.ssl) {
  // Assumes certificates are in a .ssl folder off of the package root. Make sure 
  // these files are secured.
  server = https.createServer(
    {
      // key: fs.readFileSync(path.join(__dirname,`./ssl/${environment}/www.emosetekstil.com.tr.key`)),
      // cert: fs.readFileSync(path.join(__dirname,`./ssl/${environment}/www.emosetekstil.com.tr.crt`))
        key: fs.readFileSync(`/etc/apache2/ssl/private/www.emosetekstil.com.tr.key`),
        cert: fs.readFileSync(`/etc/apache2/ssl/private/www.emosetekstil.com.tr.crt`)
      // key: fs.readFileSync(`/etc/letsencrypt/live/mail.emosetekstil.com.tr/privkey.pem`),
      // cert: fs.readFileSync(`/etc/letsencrypt/live/mail.emosetekstil.com.tr/fullchain.pem`)
      // key: fs.readFileSync(`/cert/key.key`),
      // cert: fs.readFileSync(`/cert/cert.crt`),
    },
    app  
  )
} else {
  server = http.createServer(app)
}
// Süreli Görev
setInterval(async() => {
      const contract= await Models.contract.findAll();
      JSON.stringify(contract);
      contract.forEach(async val=>{
        let end =  moment(val.enddate,'DD-MM-YYYY').toDate()
        const dif= moment().diff(end)
        console.log(dif);
        if (val.state=='Aktif' && dif > 0) {
           await Models.contract.update({state:'Pasif'},{where:{contractid:val.contractid}}).then(async data=>{
            console.log("başarılı");
          })
        }
      })
      
}, 86400000);

apollo.installSubscriptionHandlers(server);

const startApp=async()=>{
  try {
server.listen({ port: config.port }, (error) =>{
  console.log(`🚀 Server ready at http${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.graphqlPath}`);
  console.log(`🚀 Server Subscriptions at ws${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.subscriptionsPath}`);
  // logger.info('')
  // console.log(error);
  // success({
  //   message: `🚀 Server ready at http${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.graphqlPath}`,badge:true}),
  // success({
  //   message: `🚀 Server Subscriptions at wss${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.subscriptionsPath}`,
  //   badge:true
  // })
})
} catch (err) {
  error({
    badge: true,
    message: err.message
  });
  // console.error("wewe",err)
}
}
startApp();