/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js
const path = require('path')
const fs = require('fs')

module.exports = function (ctx ) {
  // console.log(path.join(__dirname));
  return {
    // https://quasar.dev/quasar-cli/supporting-ts
    supportTS: false,

    // https://quasar.dev/quasar-cli/prefetch-feature
    // preFetch: true,

    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/boot-files
    boot: [
      'apollo',
      'i18n',
      'axios',
      'turkishUpper',
      'numberandot',
      // 'moment'
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: [
      'app.styl'
    ],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
       'mdi-v5',
      // 'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'line-awesome',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons', // optional, you are not bound to it
    ],

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
       
      env: require('dotenv').config().parsed,
      scopeHoisting: true,
      vueRouterMode: 'history', // available values: 'hash', 'history'
      showProgress: true,
      // BASE_URL:'/',
      // publicPath:ctx.dev
      // ? '' :  'https://www.eriza.com.tr/',
      // publicPath:'/eriza',
      // publicPath:!ctx.dev
      // ? '' :  '/',
      env: ctx.dev
    ? { // so on dev we'll have
      KURAPI:'http://localhost:4000/',
      API:'http://localhost:4000/graphql',
      // API:'http://46.196.104.210:4000/graphql',
      WAPI:'ws://localhost:4000/graphql',
      // JWT_SECRET:JSON.stringify('emose2-app3-jwt4')
    }
    : { // and on build (production):


      
      
      // API: JSON.stringify('https://34.71.100.141:443/graphql'),
      // KURAPI:'http://188.132.204.107:42942/',
      KURAPI:'http://46.196.104.210:4000/',
      // API:'http://188.132.204.107:42942/graphql',
      API:'http://46.196.104.210:4000/graphql',
      
      // WAPI:JSON.stringify('ws://localhost'),
      
      // WAPI:JSON.stringify('wss://34.71.100.141:443/graphql'),
      // WAPI:'ws://188.132.204.107:42942/graphql',
      WAPI:'ws://46.196.104.210:4000/graphql',
      // JWT_SECRET:JSON.stringify('emose2-app3-jwt4')
    },
      // transpile: false,

      // Add dependencies for transpiling with Babel (Array of string/regex)
      // (from node_modules, which are by default not transpiled).
      // Applies only if "transpile" is set to true.
      // transpileDependencies: [],

      // rtl: false, // https://quasar.dev/options/rtl-support
      // preloadChunks: true,
      // showProgress: false,
      // gzip: true,
      // analyze: true,

      // Options below are automatically set depending on the env, set them if you want to override
      // extractCSS: false,

      // https://quasar.dev/quasar-cli/handling-webpack
      // extendWebpack (cfg, { isServer, isClient }) {
      //   cfg.resolve.alias = {
      //     ...cfg.resolve.alias, // This adds the existing alias

      //     // Add your own alias like this
      //     uploads: path.resolve(__dirname, './src/asstes/uploads'),
      //   }
      // }
      extendWebpack (cfg, { isServer, isClient }) {
        // console.log("aliases",cfg.resolve.alias);
        // console.log('path.resolve',path.resolve(__dirname));
        // console.log("module",cfg.module.rules[3].use);
        // cfg.resolve.alias = {
        //   ...cfg.resolve.alias, // This adds the existing alias

        //   // Add your own alias like this
        //   // uploads: path.resolve(__dirname, '../uploads/'),
        //   // avatar: path.resolve(__dirname, 'app/uploads/avatar/'),
        //   // wavatar: path.resolve(__dirname, '../../uploads/avatar'),

        // }
        // cfg.module.rules=[
        //   ...cfg.module.rules,
        //   {
        //     test: /\.(pdf|txt)$/,
        //     use: [
        //       {
        //         loader: 'file-loader',
        //         options: {
        //           esModule: false,
        //           name: '[path][name].[ext]'
        //         }
        //       }
        //     ]
        //   }
        // ]

      },
      // chainWebpack: config => {
      //   config.module
      //     .rule("pdf")
      //     .test(/\.pdf$/)
      //     .use("file-loader")
      //     .loader("file-loader")
      //     .end();
      //   }
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https: false,
      // https: {
        
      //   key: fs.readFileSync('./localhost.key'),
      //   cert: fs.readFileSync('./localhost.crt'),
        
      // },
      port: 8080,
      open: true, // opens browser window automatically
      

    //   headers: {
    //     'Access-Control-Allow-Origin':  '*'
    //     // 'Access-Control-Allow-Methods': '*',
    //     // 'Access-Control-Allow-Headers': '*'
    // },
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      iconSet: 'material-icons', // Quasar icon set
      lang: 'tr', // Quasar language pack
      config: {},

      // Possible values for "importStrategy":
      // * 'auto' - (DEFAULT) Auto-import needed Quasar components & directives
      // * 'all'  - Manually specify what to import
      importStrategy: 'auto',
      cssAddon: true,
      // For special cases outside of where "auto" importStrategy can have an impact
      // (like functional components as one of the examples),
      // you can manually specify Quasar components/directives to be available everywhere:
      //
      // components: [],
      // directives: [],

      // Quasar plugins
      plugins: [
        
        'Notify',
        'Loading',
        'Dialog',
        'Cookies',
        'LocalStorage',
      ]
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: [],

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: 'GenerateSW', // 'GenerateSW' or 'InjectManifest'
      workboxOptions: {}, // only for GenerateSW
      manifest: {
        name: `Eriza`,
        short_name: `Eriza`,
        description: `Eriza`,
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            src: 'icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: 'icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
      hideSplashscreen: true
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: 'packager', // 'packager' or 'builder'

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'compozition-js'
      },

      // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
      nodeIntegration: true,

      
    }
  }
}
