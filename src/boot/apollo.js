import Vue from 'vue'
import fetch from 'node-fetch'
import {LocalStorage} from 'quasar'
import { ApolloClient } from 'apollo-client'
import {   HttpLink } from 'apollo-link-http'
import {  InMemoryCache } from 'apollo-cache-inmemory'
// import { createUploadLink } from 'apollo-upload-client'
import {   onError } from "apollo-link-error"
import { ApolloLink, concat, split } from 'apollo-link'
import {setContext} from 'apollo-link-context'
import { WebSocketLink } from 'apollo-link-ws'
import {  getMainDefinition } from 'apollo-utilities'
const { createUploadLink,formDataAppendFile } = require('apollo-upload-client')
import VueApollo from 'vue-apollo'
import {  Cookies } from "quasar"
import jwt from "vuejs-jwt"
Vue.use(jwt)
import store from '../store/index'
// import fetch from 'node-fetch'
const token = (LocalStorage.getItem("token"));

// const uploadlink =createUploadLink({ uri: 'http://localhost:4000/graphql',fetch: fetch });
// const httpLink = apolloUploadClient.createUploadLink({
//   uri: 'http://'+ process.env.API +':4000/graphql',
//       credentials:'include'
// });
//  console.log(localStorage.getItem('token'));
// const uploadlink = new HttpLink({
//   // You should use an absolute URL here
//   uri: process.env.API,
//   fetch: fetch
// })
const httpLink = new HttpLink({
  // You should use an absolute URL here
  uri: process.env.API,
  fetch: fetch
})
// const httpOptions = {
//   uri: process.env.API
// }
// const httpLink = ApolloLink.split(
//   operation => operation.getContext().hasUpload,
//   createUploadLink(httpOptions),
//   // new BatchHttpLink(httpOptions)
// )
const wsLink = process.browser ? new WebSocketLink({
  uri: process.env.WAPI,
  options: {
    reconnect: true,
  },
}) : null;
const uploadlink = createUploadLink({
  uri: process.env.API,
  credentials:"same-origin",
  headers: {
    "keep-alive": "true",
  },
})
// const wsLink = new SubscriptionClient('wss://', {}, WebSocket);
// const authMiddleware =   split((operation, forward) => {
//   // add the authorization to the headers
//   const token = Cookies.get('token')
//   operation.setContext({
//     headers: {
//       // authorization: token ? `Bearer ${token}` : null
//       authorization: token ? `${token}` : null
//     }
//   })

//   return forward(operation)
// })
const linkall = process.browser ? split(({query}) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
  },
  wsLink,
  httpLink,
  uploadlink,
  // uploadlink
  
  // authMiddleware
) : httpLink;

// const authLink = new ApolloLink((operation, forward) => {
//   // console.log(operation);
//   // add the authorization to the headers
//   operation.setContext({
//     headers: {
//       authorization: `Bearer ${token}` ,
//     }
//   });
//   return forward(operation);
// })
const authLink = setContext((_, { headers, ...context}) => {
  return {
    headers: {
      ...headers,
      // ...context,
      // ibo:"ibo"
      authorization: token ? `Bearer ${token}` : '',
    }
  };
});
const errorLink = onError(({
  graphQLErrors,
  networkError
}) => {
  if (graphQLErrors)
    graphQLErrors.map(({
        message,
        locations,
        path
      }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    )
  if (networkError) console.log(`[Network error]: ${networkError}`)
})
// Create the apollo client

// createUploadLink({uri: process.env.API})

// console.log(authLink);
export const apolloClient = new ApolloClient({
  
  // link: authLink.concat(linkall, errorLink),
  link:
  // ApolloLink.from([
    ApolloLink.from([authLink,linkall]),
    // authLink.concat(linkall, uploadlink),
  // ]),
  cache: new InMemoryCache({
  // addTypename: true
}),
  connectToDevTools: true,
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors", graphQLErrors);
    console.log("networkError", networkError);
  }
  //  fetchOptions:{
  //    credentials:'include'
  //  },
  //  request:operation=>{
  //    headers:{
  //      authorization:localStorage.getItem('token')
  //    }
  //  }

});
 export const apolloProvider = new VueApollo({
   
  defaultClient: apolloClient,
  // defaultOptions: {
    // See 'apollo' definition
    // For example: default query options
    // $query: {
    //   loadingKey: 'loading',
    //   fetchPolicy: 'cache-and-network',
    // },
  // },
  // watchLoading(isLoading, countModifier) {
  //     loading += countModifier
  //     console.log('Global loading', loading, countModifier)
  //   },
    // Global error handler for all smart queries and subscriptions
    errorHandler(error) {
      console.log('Global error handler')
      console.error(error)
    },
  // loadingKey: 'loading',
  // errorHandler ({ graphQLErrors, networkError }) {
  //   if (graphQLErrors) {
  //     graphQLErrors.map(({ message, locations, path }) =>
  //       console.log(
  //         `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
  //         //  `[GraphQL error]: Message: Ql Failed!`

  //       )
  //     )
  //   }
  //   if (networkError) {
  //     console.log(`[Network error]: ${networkError}`)
  //     //  console.log(`[Network error]: "Network Failed"`)

  //   }
  // }
  // errorHandler (error) {
  //   console.log('Global error handler')
  //   console.error(error)
  // },
});
export default ({
  app,
  
  Vue
}) => {
  Vue.use(VueApollo);
  

  app.apolloProvider = apolloProvider
}
