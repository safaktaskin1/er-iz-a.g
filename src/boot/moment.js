import Vue from 'vue';
import moment from 'moment';

moment.locale('tr');

export default function install (Vue) {
  Object.defineProperties(Vue.prototype, {
    $moment: {
      get () {
        return moment;
      }
    }
  })
}

// main.js

// import moment from './plugins/moment.js';
// Vue.use(moment);