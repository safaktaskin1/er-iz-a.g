
import Vue from 'vue'
export function turkishUpper(b) { 
  var letters = { "i": "İ", "ş": "Ş", "ğ": "Ğ", "ü": "Ü", "ö": "Ö", "ç": "Ç", "ı": "I" };
  b = b.replace(/(([iışğüçö]))/g, function(letter){ return letters[letter]; }).toUpperCase()
  return b
 }
 Vue.prototype.$turkishUpper = turkishUpper