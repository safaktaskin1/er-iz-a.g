import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
var jwt = require('../../node_modules/jsonwebtoken');
// require('../../node_modules/dotenv').config(); 
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
};
Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeEach( (to, from, next) => {
    // console.log(process.env.JWT_SECRET);
    // console.log("to",to.path);
    if (process.browser) {
      // console.log("if:1");
    let  authorize =[] 
   const token =localStorage.getItem('token')
    // console.log("routes",to.matched.some(record => authorize = record.meta.authorize));
    
   if(to.matched.some(record=>record.meta.authorize) ){
    to.matched.some(record => authorize = record.meta.authorize)
    console.log("if:1");
    jwt.verify(token, process.env.JWT_SECRET ,async function(err, token)  {
      if(err){
        console.log("if:2");
        next('/login')
      //  return
      }else{
        console.log("else:3");
        // console.log((token.role));
        // console.log(!authorize.includes(token.role));
        if (!authorize.includes(token.role)) {
          // yetki yoksa
          console.log("if:4");
          // console.log(authorize.includes(token.role));
          // console.log("!ai");
          // localStorage.setItem('email',token.email)
          // return next()
          next('/ticket')
        }else{
          console.log("else:5");
          return next()
      //     return next({
      //   path:'/admin'
      // })
        }
      }
    })
   }else{
    console.log("else:6");
    //  // izin gerekmez ise admin user ise ticketa değilse logine
    // console.log(token);
     if (token == null) {
      console.log("if:7");
      // return next({
      //   path:'/login'
      // })
     }else {
      console.log("else:8");
       jwt.verify(token, process.env.JWT_SECRET ,async function(err, token)  {
            if(err){
              console.log("if:9");
              localStorage.removeItem('token')
              localStorage.removeItem('email')
              localStorage.removeItem('firstname')
              return next({
                path:'/login'
              })
             }else{
              console.log("else:10");
              // if (token.role == "admin" || token.role == "superuser"|| token.role == "orderuser"|| token.role == "user") {
              if ( token.role == "user") {
                console.log("if:11");
                return next({
                  path:'/ticket'
                })
              }
              else{
                console.log("else:12");
                // return next()
                return next({
                path:'/admin'
              })
              }
             }
       })
       
     }
   }
   next()
  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-
  //  if(to.matched.some(record=>record.meta.authorize)){
  //   
  //        jwt.verify(token, process.env.JWT_SECRET ,async function(err, token)  {
  //      if(err){
  //       next('/login')
  //      return
  //     }else{
  //      if (authorize.length && !authorize.includes(token.role)) {
        
  //        console.log("routes token",token);
  //        localStorage.setItem('email',token.email)
  //         if (token.role == "admin" || token.role == "superuser") {
  //           return next({
  //             path: '/admin'
  //           });
  //         }else{
  //           return next({
  //             path: '/ticket'
  //           });
  //         }
  //    }
  //     }
  //  });
  //  }
  //  next()
  // }else{
  //   next()

   }
 })
  return Router
}
