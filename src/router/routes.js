
const routes = [

  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [{
        path: '',
        component: () => import('pages/login/login.vue')
      }]
  },
  {
    path: '/denememail',
    component: () => import('layouts/mail.vue'),
    // children: [{
    //     path: '',
    //     component: () => import('pages/login/login.vue')
    //   }]
  },
  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/login/login.vue')},
      { path: '/forgotpassword',component: () => import('pages/login/forgotpage.vue') },
      { path: '/changepassword',component: () => import('pages/login/newpassword.vue'),
        props: route => ({ token: route.query.token })
      },
    ]
  },
  {
    path: '/ticket',
    meta: {
      authorize: ["user","admin","superuser","orderuser"]
    },
    component: () => import('layouts/LoginLayout.vue'),
    children: [
        { path: '',component: () => import('pages/login/ticket.vue') },
        { path: '/settings',component: () => import('pages/login/settings.vue') },

      ]
  },
  {
    path: '/admin',
    meta: { authorize: ["superuser", "admin"] },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/admin/Index.vue') },
      { path: '/transactions', component: () => import('pages/admin/jobs/transactions.vue') },
      { path: '/category', component: () => import('pages/admin/Category.vue') },
      { path: '/companydetail', component: () => import('pages/admin/Company.vue') },
      { path: '/user', component: () => import('pages/admin/User.vue') },
      { path: '/department', component: () => import('pages/admin/Department.vue') },
      { path: '/userdepartment', component: () => import('pages/admin/UserDepartment.vue') },
      { path: '/companyuser', component: () => import('pages/admin/UserCompany.vue') },
      { path: '/jobs', component: () => import('src/pages/admin/jobs/jobs.vue') },
      { path: '/waitjobs', component: () => import('src/pages/admin/jobs/waitjobs.vue') },
      { path: '/activejobs', component: () => import('src/pages/admin/jobs/activejobs.vue') },
      { path: '/newticket', component: () => import('src/pages/admin/jobs/newticket.vue') },
      { path: '/transferjobs', component: () => import('src/pages/admin/jobs/transferjobs.vue') },
      { path: '/products', component: () => import('src/pages/admin/products.vue') },
      { path: '/productscategory', component: () => import('src/pages/admin/productscategory.vue') },
      { path: '/contract', component: () => import('src/components/contract.vue') },
      { path: '/plan', component: () => import('src/components/plan.vue') },
      { path: '/changepassword',component: () => import('pages/login/newpassword.vue'),
        props: route => ({ token: route.query.token })
      },
      { path: '/admin/settings',name:'adminsettings', component: () => import('src/pages/admin/settings.vue') },
      { path: '/admin/pagevariables',name:'pagevaariables', component: () => import('src/pages/admin/pagevariables.vue') },





      // -*-*-*-*-*-*-*-*-*-**
      { path: '/deneme', component: () => import('src/pages/deneme.vue') },
      { path: '/design', component: () => import('src/pages/admin/design.vue') },

      // { path: '/user', component: () => import('pages/admin/User.vue') },

    ]
  },
  {
    path: '/admin',
    meta: { authorize: ["orderuser", "admin", "superuser"] },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/checkedendjobs', component: () => import('pages/admin/jobs/checkedendjobs.vue') },
      { path: '/notcheckedendjobs', component: () => import('pages/admin/jobs/notcheckedendjobs.vue') },
      // { path: '/user', component: () => import('pages/admin/User.vue') },
    ]
  },



  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
