import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import gql from 'graphql-tag';
import {apolloClient} from '../../boot/apollo';
import {Loading} from 'quasar';
import main from './mainadmin' 



export default{
  state:{
    alljobs:[],
    departmentsjobs:[],
    waitjobs:[],
    activejobs:[],
    transferjobs:[],
    sendtransferjobs:[],
    endjobs:[],
    endcheckedjobs:[],
    canceljobs:[],
    extrajobstate:[]
  },
  actions:{
    async alljobsAction({
        
      commit,
      state
    },type ) {
      let self =this
      // Loading.show()
      let an=await axios.post(
          process.env.API, {
        // await apolloClient.query({
            query: `{
              alljobsQuery {
                jobsid
                priority
                title
                description
                time
                amount
                outjobstate
                createdAt
                updatedAt
                uploads{
                    uploadid
                    path
                    jobsid
                  }
                jobsrow{
                  jobsrowid
                  productid
                  jobsid
                  pieceunit
                  piece
                  unitprice
                  currencyunit
                  currency
                  createdAt
                  amount
                  
                  product{
                    productid
                    name
                    serino
                    productcode
                    pieceunit
                    buyingprice
                    buyingexchange
                    priceunitmodel
                  }
                }
                outjobs{
                  outjobid
                  jobsid
                  userid
                  companyid
                  outjobstate
                  note
                  path
                  uploads{
                    outjobid
                    name
                    path
                  }
                  company{
                    companyid
                    name
                  }
                  user{
                    email
                  }
                  updatedAt
                  createdAt
                }
                jobstate{
                  jobstateid
                  state
                  active
                  userid
                  transferuserid
                  transferstateid
                  jobsid
                  note
                  createdAt
                  stateuser{
                    userid
                    firstname
                    email
                    }
                  transferuser{
                    userid
                    firstname
                    email
                  }
                  }
                openuser{
                  userid
                  firstname
                  email
                }
                company{
                  name
                  contracts{
                    contractid
                    companyid
                    startdate
                    enddate
                    state
                    contractrows{
                      contractrowid
                      productid
                      count
                      balancecount
                    }
                  }
                }
                  category{
                    name
                    categoryid
                    departmentcategory{
                      department{
                        name
                      }
                    }
                  } 
                }
      }`,
          })
          axios.all([
            an
          ]).then(async res=>{
            // console.log("allJobs",res[0].data.data.alljobsQuery);
            let depjob = await res[0].data.data.alljobsQuery
            // console.log("allactions",depjob);
            await commit('setalljobs', depjob)
            await commit('settransferjobs', depjob)
            // await commit('setendjobs', depjob)
            // -*-*-*-*-*-*
            await commit('setextrajobstate')
            // Loading.hide()
          })
        },
    async departmentsjobs({commit,state}, type) {
        let self =this
        let as =[]
        console.log(main.state.userCategory);
        // console.log(main.state.userdetail.userdepartments);
        // Loading.show()
        main.state.userdetail.userdepartments.forEach(val=>{
          as.push({departmentid:val.departmentid})
        })
        //  console.log("as",as);
        let dep=await axios.post(
        process.env.API, {
        //  apolloClient.query({
          query:  `query departmentjobsQuery($list:[departmentInput]){
               departmentjobsQuery(list:$list){
                jobsid
                priority
                title
                description
                time
                amount
                outjobstate
                createdAt
                updatedAt
                uploads{
                  uploadid
                  path
                  jobsid
                }
                jobsrow{
                  jobsrowid
                  productid
                  jobsid
                  pieceunit
                  piece
                  unitprice
                  currencyunit
                  currency
                  createdAt
                  amount
                  product{
                    productid
                    name
                    serino
                    productcode
                    pieceunit
                    buyingprice
                    buyingexchange
                    priceunitmodel
                  }
                }
                outjobs{
                  outjobid
                  jobsid
                  userid
                  companyid
                  outjobstate
                  note
                  path
                  uploads{
                    outjobid
                    name
                    path
                  }
                  company{
                    companyid
                    name
                  }
                  user{
                    email
                  }
                  updatedAt
                  createdAt
                }
                jobstate{
                  jobstateid
                  state
                  active
                  userid
                  transferuserid
                  transferstateid
                  jobsid
                  note
                  createdAt
                  stateuser{
                    userid
                    firstname
                    email
                    }
                  transferuser{
                    userid
                    firstname
                    email
                  }
                  }
                openuser{
                  userid
                  firstname
                  email
                }
                company{
                  name
                  contracts{
                    contractid
                    companyid
                    startdate
                    enddate
                    state
                    contractrows{
                      contractrowid
                      productid
                      count
                      balancecount
                    }
                  }
                }
                  category{
                    name
                    categoryid
                    departmentcategory{
                      department{
                        name
                      }
                    }
                  } 
               }
    }`,
    variables:{
      // list:main.state.userdetail.userdepartments
      list:as
    }
        })
        axios.all([
          dep
        ]).then(async res=>{
          // console.log(res[0].data.data.alljobsQuery);
          
          let depjob = res[0].data.data.departmentjobsQuery
            // console.log("depjob",depjob);
            await commit('setdepartmentsjobs', depjob)
            await commit('setwaitjobs', depjob)
            await commit('setactivejobs', depjob)
            await commit('setsendtransferjobs', depjob)
            
            // await commit('setendcheckedjobs', depjob)
            // await commit('setalljobs', depjob)
            // Loading.hide()
          //
          //
        })
        },
    async endjobs({ commit, state },type ) {
      Loading.show()
      await axios.post(
        process.env.API, {
          query: `{
            endjobsQuery {
              jobsid
              priority
              title
              description
              time
              amount
              outjobstate
              createdAt
              updatedAt
              uploads{
                  uploadid
                  path
                  jobsid
                }
              jobsrow{
                jobsrowid
                productid
                jobsid
                pieceunit
                piece
                unitprice
                currencyunit
                currency
                createdAt
                amount
                
                product{
                  productid
                  name
                  serino
                  productcode
                  pieceunit
                  buyingprice
                  buyingexchange
                  priceunitmodel
                }
              }
              outjobs{
                outjobid
                jobsid
                userid
                companyid
                outjobstate
                note
                path
                uploads{
                  outjobid
                  name
                  path
                }
                company{
                  companyid
                  name
                }
                user{
                  email
                }
                updatedAt
                createdAt
              }
              jobstate{
                jobstateid
                state
                active
                userid
                transferuserid
                transferstateid
                jobsid
                note
                createdAt
                stateuser{
                  userid
                  firstname
                  email
                  }
                transferuser{
                  userid
                  firstname
                  email
                }
                }
              openuser{
                userid
                firstname
                email
              }
              company{
                name
                contracts{
                  contractid
                  companyid
                  startdate
                  enddate
                  state
                  contractrows{
                    contractrowid
                    productid
                    count
                    balancecount
                  }
                }
              }
                category{
                  name
                  categoryid
                  departmentcategory{
                    department{
                      name
                    }
                  }
                } 
              }
          }`,
        })
        .then(async res => {
          // console.log('endjobs',res);
          Loading.hide()
          // await commit('setendjobs', res.data.data.endjobsQuery)
          state.endjobs=res.data.data.endjobsQuery
          
        })
    }
    
  
  },
  mutations:{
    setalljobs(state, type) {
      // console.log(type);
      state.alljobs = type
    },
    async setextrajobstate(state, type) {
      // console.log(type);
      await axios.post(
        process.env.API, {
          query: `{
            extrajobstate {
              extrajobstateid
              name
                  
                }
          }`,
        }).then(async data => {
          // console.log('data',data.data.data.extrajobstate);
          state.extrajobstate = data.data.data.extrajobstate
        })
      
    },
    setdepartmentsjobs(state, type) {
      state.departmentsjobs = type
    },
    setwaitjobs(state, type) {
      // console.log("wt",type);
      let arrwait
      arrwait=type.filter((vala)=>{
        // console.log("text:",vala.jobstate.length);
        return vala.jobstate.length == 0
        }
          
        
        // else{
        //   vala.jobstate.filter((valb)=>{
        //     if (valb.state=='Beklemede' && valb.active) {
        //       return vala;
        //     }
        //   })
        // }
      )
      // console.log("vuex wait",type);
      state.waitjobs = arrwait
    },
    setactivejobs(state, sval) {
  
      // let smal= sval.filter( (vala,ia)=>{
      //   if (vala.jobstate.length != 0) {
      //     let count=0
      //     vala.jobstate= vala.jobstate.filter(async (valb,ib)=>{
            
      //       if (main.state.userdetail.userid==valb.userid && valb.state=='Aktif' && valb.active ) {
      //         count++
      //         return valb
      //       }
      //     })
      //     if (count > 0) {
      //       return vala
            
      //     }
      //   }
        
      // })
        let filteredArray = sval
        .filter((element) => 
          element.jobstate.some( (t) =>(t.state == 'Aktif'  &&  t.active && main.state.userdetail.userid == t.userid) ))
        .map(element => {
          let newElt = Object.assign({}, element); 
          return newElt
        });
      //  console.log("setend",filteredArray);
      state.activejobs=filteredArray
      // console.log("vuex active",filteredArray);
        // state.activejobs = smal
      
    },
    settransferjobs(state, type) {
      

      let mal= type.filter( (vala,ia)=>{
        if (vala.jobstate.length != 0) {
          // console.log("vala",ia);
          let count1=0
          vala.jobstate= vala.jobstate.filter(async (valb,ib)=>{
            
            //  console.log(main.state.userdetail.userid,":",valb.userid, valb.state,":",'Transfer',valb.active, valb.jobsid );
            if (main.state.userdetail.userid==valb.userid && valb.state=='Transfer'  && valb.active ) {
              // console.log(valb);
              count1 ++
              return valb
            }
          })
          if (count1 > 0) {
            return vala
          }
        }
      })
      
  
      //    let settransfer=type.filter((element) => 
      //   element.jobstate.some((t) => t.state == 'Transfer' && main.state.userdetail.userid == t.userid &&  t.active))
      //   .map( element => {
      //   let newElt = Object.assign({}, element); 
      //   return newElt
      // })
      
      // let settransfer = type.filter((element) =>element.jobstate.filter(async (v)=>v.state == 'Transfer'&& main.state.userdetail.userid == t.userid))
      //  console.log("settransfer",mal);
      state.transferjobs=mal
    },
    setsendtransferjobs(state, trval) {
      
      // console.log(trval);
        let sstval=trval.filter( (vala,ia)=>{
           
          if (vala.jobstate.length != 0) {
            // console.log("vala",ia);
            let count1=0
            vala.jobstate=vala.jobstate.filter(async(valb,ib)=>{
              // console.log(valb);
              // console.log(state.userdetail.userid==valb.userid );
              if (main.state.userdetail.userid==valb.userid && valb.state=='Transfer' && !valb.active ) {
                
                count1 ++
                return valb
              }
            })
            if (count1 > 0) {
              
              return vala
            }
          }
          
        })
        // console.log("sendtransferjobs",sstval);
        state.sendtransferjobs=sstval
    },
    setendjobs(state,type){
    
    // const filteredArrayy = type.filter((element) => 
    //   element.jobstate.some((t) => t.state == 'Tamamlandı'  &&  t.active))
    // .map( element => {
    //   const newElt = Object.assign({}, element); 
    //   return newElt
    // });
    //  console.log("setend",filteredArray);
    // Loading.hide()
        state.endjobs=type
    },
    setendcheckedjobs(state,type){
      // console.log("setendcheckedjobs",type);
      const filteredArray = type
    .filter((element) => 
      element.jobstate.some((t) =>t.state == 'Onaylandı'  &&  t.active))
    .map( element => {
      const newElt = Object.assign({}, element); 
      return newElt
    });
    // let filteredArray= type.filter( (vala,ia)=>{
    //   if (vala.jobstate.length != 0) {
    //     // console.log("vala",ia);
    //     let count1=0
    //     vala.jobstate= vala.jobstate.filter(async (valb,ib)=>{
          
    //       //  console.log(main.state.userdetail.userid,":",valb.userid, valb.state,":",'Transfer',valb.active, valb.jobsid );
    //       if ( valb.state=='Onaylandı' && valb.active ) {
    //         // console.log(valb);
    //         count1 ++
    //         return valb
    //       }
    //     })
    //     if (count1 > 0) {
    //       return vala
    //     }
    //   }
    // })
      // console.log("setendcheckedjobs",filteredArray);
        state.endcheckedjobs=filteredArray
    },
    setcanceljobs(state,type){
    
    let filteredArray = type
    .filter((element) => 
      element.jobstate.some((t) =>t.state == 'İptal' && main.state.userdetail.userid == t.userid &&  t.active))
    .map(element => {
      let newElt = Object.assign({}, element); 
      return newElt
    });
    //  console.log(sttype);
        state.canceljobs=filteredArray
    }
  },
  getters:{
    getalljobs: (state, getters) => {
      return state.alljobs;
    },
    getdepartmentsjobs: (state, getters) => {
      return state.departmentsjobs;
    },
    getwaitjobs: (state, getters) => {
      return state.waitjobs;
    },
    getactivejobs: (state, getters) => {
      return state.activejobs;
    },
    gettransferjobs: (state, getters) => {
      return state.transferjobs;
    },
    getsendtransferjobs: (state, getters) => {
      return state.sendtransferjobs;
    },
    getendjobs: (state, getters) => {
      return state.endjobs;
    },
    getendcheckedjobs: (state, getters) => {
      return state.endcheckedjobs;
    },
    getcanceljobs: (state, getters) => {
      return state.canceljobs;
    },
    getextrajobstate: (state, getters) => {
      return state.extrajobstate;
    },
  }
}