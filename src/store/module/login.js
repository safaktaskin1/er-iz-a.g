import Vue  from 'vue'
import axios from 'axios'
import gql from 'graphql-tag'

export default {
    state: {
        user:{},
        ticketcategory:{}
    },
    actions:{
        async user({dispatch,commit},type) {
            commit('set_user',type);
        },
        logout({
          commit
        }) {
          localStorage.removeItem("token")
          localStorage.removeItem("firstname")
          localStorage.removeItem("email")
        },

    },
    mutations:{
        set_user(state,type){
          // console.log(type);
            state.user=type
        },
        
            set_ticketcategory(state, type) {
              state.ticketcategory = type
            },
    },
    getters:{
        get_user: (state, getters) => {
            return state.user
          },
        get_ticketcategory: (state, getters) => {
            return state.ticketcategory
          },
    }
}