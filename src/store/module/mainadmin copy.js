import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import gql from 'graphql-tag';
import {apolloClient} from '../../boot/apollo';
import {Loading} from 'quasar';
import state from '../module-example/state';
import { assign } from 'apollo-utilities';
import login from './login.js';
import { setTimeout } from 'core-js';
var _ = require('lodash');
// Vue.use(Vuex);

export default{
  state: {
    // categoryid:"",
    treem:[],
    category:[],
    allCategory:[],
    allUser:[],
    adminUsers:[],
    allCompany:[],
    allDepartment:[],
    alljobs:[],
    userdetail:{},
    departmentsjobs:[],
    waitjobs:[],
    activejobs:[],
    transferjobs:[],
    endjobs:[],
    canceljobs:[]
  },
  actions:{
    async treedata({
      
        dispatch,
        commit
      }, type) {
        // Loading.show()
        await apolloClient.query({
          query: gql ` query  {
                      categoryQuery{
                        name
                        parentid
                        categoryid
                      
                      }
                      }`,
            // variables: { }
        })
          .then(async data => {
          // Loading.hide()
          // console.log(data);
          let selfQ = data.data.categoryQuery
          selfQ.forEach((aval,ai)=>{
            selfQ.forEach((bval,bi)=>{
              if (aval.parentid == null) {
                Vue.set(selfQ[ai], 'parentname', 'ANA KATEGORİ')
              }
              else if (aval.parentid==bval.categoryid) {
                // console.log(aval.parentid);
                
                // console.log(bval.name);
                Vue.set(selfQ[ai], 'parentname', bval.name)
                
              }
            })
          })
          await Vue.nextTick(()=>{
            commit('setcategory', selfQ)
            // state.category=selfQ;
          })
           
          // console.log('state.category', state.category);
           parseTree(selfQ);
             let treeem=[];
             function parseTree(selfQ, parentid = null) {
              let treem = [];
                // console.log(selfQ);

              selfQ.forEach((value, index) => {
                // console.log('value:', value);
                if (value.parentid === parentid) {
                  // console.log(value);
                  // const children=[]
                  const children = parseTree(selfQ, value.categoryid);
                  //  console.log('children:', children);
                  if (children) {
                      // console.log('1');
                    // value=Object.assign({}, {isChildren:true});
                    Vue.set(value, "children", children);
                    // Vue.set(value,"isChildren",true)
                  } else {
                    // Vue.set(value, "isChildren", false)
                    Vue.set(value, 'children', [])
                  }
                  treem.push(value);
                }
              });
              //  console.log('treem',treem);
              Vue.nextTick(()=>{
                commit('treem', treem)
              })
              
               
              //  commit('setcategory', selfQ)
               return treem;
            }

            // console.log('treem2', treeem);
            // parseTree(selfQ);
        }).catch(err => {
          console.log(err);
          Loading.hide()
        })
      },
      async allCategory({
          dispatch,
          commit
        }, type) {
          apolloClient.query({
              query: gql `query{
                    allCategory {
                      name
                      parentid
                      categoryid
                      children{
                        name
                        parentid
                        categoryid
                      }
                    }
        }`,
              }).then(async data => {
                // let anaktegori={
                //   categoryid:0,
                //   parentid:null,
                //   name:'ANA KATEGORİ',
                //   parentname:'ANA KATEGORİ'
                // }
                let arr = data.data.allCategory
                // arr.push(anaktegori)
                // console.log(arr);
                commit('setallCategory', arr)
              })
        },
    async allUser({
        dispatch,
        commit
      }, type) {
        await apolloClient.query({
              query: gql `query{
                    allUser {
                      userid
                      firstname
                      lastname
                      email
                      role
                      res
                      cuser {
                        companies {
                          name
                        }
                      }
                    }
        }`,
            }).then(async data => {
              commit('setallUser', data.data.allUser)
              
            })
          },
    async userdetail({
        dispatch,
        commit
      }, type) {
        await apolloClient.query({
              query: gql `query userdetail($email:String){
                    userdetail(email:$email) {
                      
                      userid
                      firstname
                      lastname
                      email
                      role
                      res
                      cuser {
                        companies {
                          companyid
                          name
                        }
                      }
                      userdepartments{
                        departmentid
                      }
                    }
        }`,
        variables:{
          email:type
        }
            }).then(async data => {
             await commit('setuserdetail', data.data.userdetail)
            })
          },
    async allCompany({
        dispatch,
        commit
      }, type) {
        await axios.post(
            process.env.API, {
              query: `{
                    companiesQuery {
                      companyid
                      name
                      il
                      cusers{
                        user{
                          userid
                          firstname
                          email
                        }
                      }
                      
                      
                    }
        }`,
            }).then(async data => {
              commit('setallCompany', data.data.data.companiesQuery)
              // console.log('setallCompany', data.data.data.companiesQuery);
            })
          },
    async allDepartment({
        dispatch,
        commit
      }, type) {
        await axios.post(
            process.env.API, {
              query: `{
                    departmentQuery {
                      departmentid
                      name
                      departmentcategories {
                        departmentcategoryid
                        categories {
                          categoryid
                          name
                        }
                      }
                    }
        }`,
            }).then(async data => {
              commit('setallDepartment', data.data.data.departmentQuery)
            })
          },
    async alljobs({
        dispatch,
        commit,
      }, type) {
        await axios.post(
            process.env.API, {
              query: `{
                alljobsQuery {
                  jobsid
                  openuser{firstname}
                  company{name}
                  category{name}
                  createdAt  
                  }
        }`,
            }).then(async data => {
              commit('setalljobs', data.data.data.alljobsQuery)
            })
          },
    async departmentsjobs({commit,state}, type) {
        // console.log("departmentjobs", state.userdetail.userdepartments);
        // if (state.userdetail.userdepartments.length > 0) 
        // {
          await axios.post(
          process.env.API, {
          // await apolloClient.query({
            query:  `query departmentjobsQuery($list:[departmentInput]){
                 departmentjobsQuery(list:$list){
                  jobsid
                  priority
                  description
                  
                  createdAt
                  updatedAt
                  jobstate{
                    jobstateid
                    state
                    active
                    transferuserid
                    jobsid
                    note
                    stateuser{
                      userid
                      firstname
                      email
                      }
                      transferuser{
                        userid
                        firstname
                        email
                      }
                    }
                  openuser{
                    userid
                    firstname
                    email
                  }
                  company{
                    name
                  }
                    category{
                      name
                      departmentcategory{
                        department{
                          name
                        }
                      }
                    }
                 }
      }`,
      variables:{
        list:state.userdetail.userdepartments
      }
          }).then(async  data => {
            
              //  console.log("departt",data.data.data.departmentjobsQuery); 
            await commit('setdepartmentsjobs', data.data.data.departmentjobsQuery)
            await commit('setwaitjobs', data.data.data.departmentjobsQuery)
            await commit('setactivejobs', data.data.data.departmentjobsQuery)
            await commit('settransferjobs', data.data.data.departmentjobsQuery)
            await commit('setendjobs', data.data.data.departmentjobsQuery)
            await commit('setcanceljobs', data.data.data.departmentjobsQuery)
           

             
            })
          },
         
  },
  mutations:{
    
      setallUser(state, type) {
      state.allUser = type
      // console.log(state.treem);
    },
    setadminUsers(state, type) {
      // console.log("type",type);
      // type=type.filter((val)=>{
        // console.log("state",console.log(state.allUser));
      //   return val.role!='user' && val.email!=console.log(state.treem);
      // })
       state.adminUsers = type
      // console.log(state.treem);
    },

      setuserdetail(state, type) {
      state.userdetail = type
      // console.log(state.treem);
    },

      treem(state, type) {
      state.treem=type
      // console.log(state.treem);
    },
      setcategory(state, type) {
      state.category = type
      // console.log(state.treem);
    },
      setallCategory(state, type) {
      state.allCategory = type
    },
      setallCompany(state, type) {
      state.allCompany = type
    },
      setallDepartment(state, type) {
      state.allDepartment = type
    },
      setalljobs(state, type) {
      state.alljobs = type
    },
    setdepartmentsjobs(state, type) {
      state.departmentsjobs = type
    },
    setwaitjobs(state, type) {
      let arrwait=type
      arrwait=arrwait.filter((vala)=>{
        if (vala.jobstate.length == 0) {
          return vala;
        }else{
          vala.jobstate.filter((valb)=>{
            if (valb.state=='Beklemede' && valb.active) {
              return vala;
            }
          })
        }
      })
      // console.log("vuex wait",type);
      state.waitjobs = arrwait
    },
     setactivejobs(state, type) {
      
    //   type=_.filter(type, function(o) { 
    //     let arr=[]
    //     let count =0
    //     if (o.jobstate.length != 0) {
    //       o.jobstate= _.filter(o.jobstate,async function(s) { 
    //         if (state.userdetail.userid==s.stateuser.userid && s.state=='Aktif' && s.active && s.transferuserid==null ) {
    //           count ++
    //           // console.log(s);
    //           return s ;
    //         }
    //       })
    //       if(count > 0  )
    //       return o
    //     }
    //  });
    
    let filteredArray = type
    .filter((element) => 
      element.jobstate.some((s) => state.userdetail.userid==s.stateuser.userid && s.state=='Aktif' && s.active && s.transferuserid==null))
    .map(element => {
      let newElt = Object.assign({}, element); 
      return newElt
    });
    // console.log(state.userdetail.userid);
        state.activejobs = filteredArray
      
    },
     settransferjobs(state, sttype) {
      // console.log("settransferjobs type",type);
      
      
      sttype=_.filter(sttype,  function(m) { 
        let countt =0
        // console.log("m",m);
        if (m.jobstate.length != 0) {
          m.jobstate= _.filter(m.jobstate, function(t) { 
            // console.log("t",t);
            if (t.state == 'Transfer' && state.userdetail.userid == t.transferuserid &&  t.active ) {
              // console.log(t);
              countt ++
              return t ;
            }
            // return [];
          })
          // console.log(m.jobstate);
          if(countt > 0  ){
          return m
        }
        }

     });
    //  console.log(sttype);
        state.transferjobs=sttype
      // }
      
      // console.log("settransferr",type);
    },
    setendjobs(state,type){
      type=_.filter(type,  function(m) { 
        let countt =0
        // console.log("m",m);
        if (m.jobstate.length != 0) {
          m.jobstate= _.filter(m.jobstate,async function(t) { 
            // console.log("t",t);
            if (t.state == 'Tamamlandı' && state.userdetail.userid == t.transferuserid &&  t.active ) {
              // console.log(t);
              countt ++
              return t ;
            }
            // return [];
          })
          if(countt > 0  )
          return m
        }
     });
    //  console.log(sttype);
        state.endjobs=type
    },
    setcanceljobs(state,type){
      type=_.filter(type,  function(m) { 
        let countt =0
        // console.log("m",m);
        if (m.jobstate.length != 0) {
          m.jobstate= _.filter(m.jobstate,async function(t) { 
            // console.log("t",t);
            if (t.state == 'İptal' && state.userdetail.userid == t.transferuserid &&  t.active ) {
              // console.log(t);
              countt ++
              return t ;
            }
            // return [];
          })
          if(countt > 0  )
          return m
        }
     });
    //  console.log(sttype);
        state.canceljobs=type
    }
  },
  getters:{
    gettreem: (state, getters) => {
      return state.treem;
    },
    getcategory: (state, getters) => {
      return state.category;
    },
    getallCategory: (state, getters) => {
      return state.allCategory;
    },
    getallUser: (state, getters) => {
      return state.allUser;
    },
    getadminUsers: (state, getters) => {
      return state.adminUsers;
    },
    get_userdetail: (state, getters) => {
      return state.userdetail;
    },
    getallCompany: (state, getters) => {
      return state.allCompany;
    },
    getallDepartment: (state, getters) => {
      return state.allDepartment;
    },
    getalljobs: (state, getters) => {
      return state.alljobs;
    },
    getdepartmentsjobs: (state, getters) => {
      return state.departmentsjobs;
    },
    getwaitjobs: (state, getters) => {
      return state.waitjobs;
    },
    getactivejobs: (state, getters) => {
      return state.activejobs;
    },
    gettransferjobs: (state, getters) => {
      return state.transferjobs;
    },
    getendjobs: (state, getters) => {
      return state.endjobs;
    },
    getcanceljobs: (state, getters) => {
      return state.canceljobs;
    },
  }
};