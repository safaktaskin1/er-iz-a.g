import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import gql from 'graphql-tag';
import {apolloClient} from '../../boot/apollo';
import {Loading} from 'quasar';
import state from '../module-example/state';
import { assign } from 'apollo-utilities';
import login from './login.js';
import { setTimeout } from 'core-js';
var _ = require('lodash');
// Vue.use(Vuex);

export default{
  state: {
    // categoryid:"",
    deneme:[],
    treem:[],
    category:[],
    allCategory:[],
    allUser:[],
    adminUsers:[],
    allCompany:[],
    allDepartment:[],
    alljobsdata:[],
    allProducts:[],
    allfilterProducts:[],
    userdetail:{},
    userCategory:[],
    contracts:[],
    allPlan:[],
    allContractDetail:[]
    
  },
  actions:{
    async treedata({
      
        dispatch,
        commit
      }, type) {
        // Loading.show()
        // await apolloClient.query({
        //   query: gql ` query  {
        //               categoryQuery{
        //                 name
        //                 parentid
        //                 categoryid
        //                 # parent{
        //                 #   name
        //                 #   parentid
        //                 # }
                      
        //               }
        //               }`,
        //     // variables: { }
        // })
        await axios.post(
          process.env.API, {
            query: `{
              categoryQuery {
                    name
                    parentid
                    categoryid
                  }
            }`,
          })
          .then(async data => {
          // Loading.hide()
          // console.log(data);
          let selfQ = data.data.data.categoryQuery
          selfQ.forEach((aval,ai)=>{
            selfQ.forEach((bval,bi)=>{
              if (aval.parentid == null) {
                Vue.set(selfQ[ai], 'parentname', 'ANA KATEGORİ')
              }
              else if (aval.parentid==bval.categoryid) {
                // console.log(aval.parentid);
                
                // console.log(bval.name);
                Vue.set(selfQ[ai], 'parentname', bval.name)
                
              }
            })
          })
          // console.log("self",selfQ);
          await Vue.nextTick(()=>{
            commit('setcategory', selfQ)
            // state.category=selfQ;
          })
           
          // console.log('state.category', state.category);
           parseTree(selfQ);
             let treeem=[];
             function parseTree(selfQ, parentid = null) {
              let treem = [];
                // console.log(selfQ);

              selfQ.forEach((value, index) => {
                // console.log('value:', value);
                if (value.parentid === parentid) {
                  // console.log(value);
                  // const children=[]
                  const children = parseTree(selfQ, value.categoryid);
                  //  console.log('children:', children);
                  if (children) {
                      // console.log('1');
                    // value=Object.assign({}, {isChildren:true});
                    Vue.set(value, "children", children);
                    // Vue.set(value,"isChildren",true)
                  } else {
                    // Vue.set(value, "isChildren", false)
                    Vue.set(value, 'children', [])
                  }
                  treem.push(value);
                }
              });
              //  console.log('treem',treem);
              Vue.nextTick(()=>{
                commit('treem', treem)
              })
              
               
              //  commit('setcategory', selfQ)
               return treem;
            }

            // console.log('treem2', treeem);
            // parseTree(selfQ);
        }).catch(err => {
          console.log(err);
          Loading.hide()
        })
      },
    async allProducts({
      dispatch,
      commit
    }, type) {
      await axios.post(
          process.env.API, {
            query: `{
              allProducts {
                    productid
                    name
                    serino
                    productcode
                    piece
                    pieceunit
                    buyingprice
                    buyingexchange
                    priceunitmodel
                    exceptional
                    ordernumber
                    buyingorderdate
                    companyid
                    uploadorder{
                      uploadorderid
                      ordernumber
                      path
                      createdAt
                    }
                    company{
                      companyid
                      name
                    }
                    res
                  }
            }`,
          }).then(async data => {
            commit('setallProducts', data.data.data.allProducts)
            commit('setallfilterProducts', data.data.data.allProducts)

            // commit('setallfilterProducts', data.data.data.allProducts) // gereksiz
            // console.log('setallCompany', data.data.data.companiesQuery);
          })
        },
    async allCategory({
          dispatch,
          commit
        }, type) {
          await axios.post(
            process.env.API, {
              query:  `{
                    allCategory {
                      name
                      parentid
                      categoryid
                      children{
                        name
                        parentid
                        categoryid
                      }
                    }
        }`,
              }).then(async data => {
                let arr = data.data.data.allCategory
                await commit('setallCategory', arr)
              })
        },
    async allUser({
        dispatch,
        commit
      }, type) {
        // await apolloClient.query({
        //       query: gql `query{
        //             allUser {
        //               userid
        //               firstname
        //               lastname
        //               email
        //               role
        //               res
        //               cuser {
        //                 companies {
        //                   name
        //                 }
        //               }
        //             }
        // }`,
        //     }).then(async data => {
        //       commit('setallUser', data.data.allUser)
              
        //     })
        await axios.post(
          process.env.API, {
            query: `{
                          allUser {
                      userid
                      firstname
                      lastname
                      email
                      role
                      res
                      cuser {
                        companies {
                          name
                        }
                      }
                    }
        }`,
            }).then(async data => {
             await commit('setallUser', data.data.data.allUser)
              
            })
          },
    async userdetail({
        dispatch,
        commit
      }, type) {
        await axios.post(
          process.env.API, {
              query:  `query userdetail($email:String){
                    userdetail(email:$email) {
                      userid
                      firstname
                      lastname
                      email
                      role
                      res
                      path
                      cuser {
                        companies {
                          companyid
                          name
                        }
                      }
                      userdepartments{
                        departmentid
                        departments{
                          name
                          departmentcategories{
                            categoryid
                          }
                        }
                      }
                    }   
        }`,
        variables:{
          email:type
        }
            }).then(async data => {
              // console.log(data.data);
             await commit('setuserdetail', data.data.data.userdetail)
            })
          },
    async userCategory({
      dispatch,
      commit,
      state
    }, type) {
      const le=[]
      state.userdetail.userdepartments.forEach(vala=>{
        vala.departments[0].departmentcategories.forEach(valb=>{
          le.push(valb.categoryid)
        })
      })
      // console.log("state-le,",le);
      state.userCategory=le
      // console.log("state-le,",state.userCategory);
    },
    async allCompany({
        dispatch,
        commit
      }, type) {
        await axios.post(
            process.env.API, {
              query: `{
                    companiesQuery {
                      companyid
                      name
                      adress
                      email
                      fax
                      pbx
                      cep
                      il
                      ilce
                      description
                      vergiDairesi
                      vergiNo
                      cusers{
                        user{
                          userid
                          firstname
                          email
                          role
                        }
                      }
                      contracts{
                        contractid
                        companyid
                        startdate
                        enddate
                        state
                        contractrows{
                          contractrowid
                          contractid
                          productid
                          count
                          balancecount
                          product{
                            name
                            serino
                          }
                        }
                        detail{
                          contractdetailid
                          plan{
                            planid
                            name
                          }
                        }
                      }
                      
                      
                    }
        }`,
            }).then(async data => {
              await commit('setallCompany', data.data.data.companiesQuery)
              // console.log('setallCompany', data.data.data.companiesQuery);
            })
          },
    async allDepartment({
        dispatch,
        commit
      }, type) {
        await axios.post(
            process.env.API, {
              query: `{
                    departmentQuery {
                      departmentid
                      name
                      departmentcategories {
                        departmentcategoryid
                        categories {
                          categoryid
                          name
                        }
                      }
                    }
        }`,
            }).then(async data => {
              commit('setallDepartment', data.data.data.departmentQuery)
            })
          },
    async allPlann({dispatch,commit}, type) {
      await axios.post(
        process.env.API, {
          query: `{
            planQuery {
                      planid
                      name
                    }
            }`,
        }).then(async data => {
          await commit('setallPlan', data.data.data.planQuery)
        })
    },
    async allContractDetail({dispatch,commit}, type) {
      await axios.post(
        process.env.API, {
          query: `{
            ContractDetailQuery {
                contractdetailid
                contractid
                planid
              }
            }`,
        }).then(async data => {
          await commit('setallContractDetail', data.data.data.ContractDetailQuery)
        })
      },
  },
  mutations:{
    
    setallContractDetail(state,type){
      state.allContractDetail=type
    },
    setallPlan(state,type){
      state.allPlan=type
    },
    setallProducts(state,type){
      state.allProducts=type
    },
    setallfilterProducts(state,a){
      //  console.log("a",a);
      let type=a.filter(v=>v.piece > 0 || v.exceptional )
      // console.log("setallfilterProducts",type);
      state.allfilterProducts=type
    },
    setallUser(state, type) {
      state.allUser = type
      // console.log(state.treem);
    },
    setadminUsers(state, type) {
      // console.log("type",type);
      // type=type.filter((val)=>{
        // console.log("state",console.log(state.allUser));
      //   return val.role!='user' && val.email!=console.log(state.treem);
      // })
       state.adminUsers = type
      // console.log(state.treem);
    },
    setuserdetail(state, type) {
      state.userdetail = type
      // console.log(state.treem);
    },
    treem(state, type) {
      state.treem=type
      // console.log(state.treem);
    },
    setcategory(state, type) {
      state.category = type
      // console.log(state.treem);
    },
    setallCategory(state, type) {
      state.allCategory = type
    },
    setallCompany(state, type) {
      state.allCompany = type
    },
    setallDepartment(state, type) {
      state.allDepartment = type
    },
      
  },
  getters:{
    gettreem: (state, getters) => {
      return state.treem;
    },
    getcategory: (state, getters) => {
      return state.category;
    },
    getallCategory: (state, getters) => {
      return state.allCategory;
    },
    getallUser: (state, getters) => {
      return state.allUser;
    },
    getadminUsers: (state, getters) => {
      return state.adminUsers;
    },
    get_userdetail: (state, getters) => {
      return state.userdetail;
    },
    getallCompany: (state, getters) => {
      return state.allCompany;
    },
    getallDepartment: (state, getters) => {
      return state.allDepartment;
    },
    getallProducts: (state, getters) => {
      return state.allProducts;
    },
    getallfilterProducts: (state, getters) => {
      return state.allfilterProducts;
    },
    getuserCategory: (state, getters) => {
      return state.userCategory;
    },
    getPlan: (state, getters) => {
      return state.allPlan;
    },
    getContractDetail: (state, getters) => {
      return state.allContractDetail;
    },
    
    
  }
};